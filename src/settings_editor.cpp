#include <QSettings>
#include "equalx_exception.h"
#include "settings_editor.h"

namespace  {
//---------------------------------------------------------------------------------------
// Factory Default Settings
//---------------------------------------------------------------------------------------
const QFont DEFAULT_EDITOR_FONT;
const int   DEFAULT_EDITOR_FONT_SIZE   = 4; // represents the index in the font combobox
const bool  DEFAULT_EDITOR_HIGHLIGHTING= true;
const bool  DEFAULT_EDITOR_WRAPPING    = true;
const bool  DEFAULT_EDITOR_COMPLETION  = true;
const bool  DEFAULT_EDITOR_COMPLETION_CASE = true;
} // end ns anon

equalx::SettingsEditor::SettingsEditor()
    : m_font(DEFAULT_EDITOR_FONT),
      m_highlighting(DEFAULT_EDITOR_HIGHLIGHTING),
      m_textwrap(DEFAULT_EDITOR_WRAPPING),
      m_completion(DEFAULT_EDITOR_COMPLETION),
      m_isCompletionCaseSens(DEFAULT_EDITOR_COMPLETION_CASE)
{

}

equalx::SettingsEditor::SettingsEditor(QSettings &file)
{
    load(file);
}

QFont equalx::SettingsEditor::font() const
{
    return m_font;
}

void equalx::SettingsEditor::setFont(const QFont &font)
{
    m_font = font;
}

bool equalx::SettingsEditor::highlighting() const
{
    return m_highlighting;
}

void equalx::SettingsEditor::setHighlighting(bool highlighting)
{
    m_highlighting = highlighting;
}

bool equalx::SettingsEditor::textwrap() const
{
    return m_textwrap;
}

void equalx::SettingsEditor::setTextwrap(bool textwrap)
{
    m_textwrap = textwrap;
}

bool equalx::SettingsEditor::completion() const
{
    return m_completion;
}

void equalx::SettingsEditor::setCompletion(bool completion)
{
    m_completion = completion;
}

bool equalx::SettingsEditor::isCompletionCaseSens() const
{
    return m_isCompletionCaseSens;
}

void equalx::SettingsEditor::setIsCompletionCaseSens(bool isCompletionCaseSens)
{
    m_isCompletionCaseSens = isCompletionCaseSens;
}

void equalx::SettingsEditor::load(QSettings &settings)
{
    settings.beginGroup(name());
    m_font = settings.value("font", DEFAULT_EDITOR_FONT).value<QFont>();
    m_highlighting = settings.value("highlighting", DEFAULT_EDITOR_HIGHLIGHTING).toBool();
    m_completion = settings.value("completion", DEFAULT_EDITOR_COMPLETION).toBool();
    m_isCompletionCaseSens = settings.value("case_sensitive_completion", DEFAULT_EDITOR_COMPLETION_CASE).toBool();
    m_textwrap = settings.value("textwrap", DEFAULT_EDITOR_WRAPPING).toBool();
    settings.endGroup();
}

void equalx::SettingsEditor::save(QSettings &settings)
{
    if(!settings.isWritable()) {
        throw equalx::Exception("SettingsEditor::save", "Settings file is not writable");
    }

    settings.beginGroup(name());
    settings.setValue("font", QVariant::fromValue(m_font));
    settings.setValue("highlighting", m_highlighting );
    settings.setValue("textwrap", m_textwrap );
    settings.setValue("completion", m_completion );
    settings.setValue("case_sensitive_completion", m_isCompletionCaseSens );
    settings.endGroup();

    settings.sync();

    if(settings.status() != QSettings::NoError){
        throw equalx::Exception("SettingsEditor::save", "Error occured while trying to save settings");
    }
}

void equalx::SettingsEditor::reset()
{
    m_font = DEFAULT_EDITOR_FONT;
    m_highlighting = DEFAULT_EDITOR_HIGHLIGHTING;
    m_textwrap = DEFAULT_EDITOR_WRAPPING;
    m_completion = DEFAULT_EDITOR_COMPLETION;
    m_isCompletionCaseSens = DEFAULT_EDITOR_COMPLETION_CASE;
}
