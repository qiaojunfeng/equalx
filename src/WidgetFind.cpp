/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "WidgetFind.h"
#include "ui_widgetFind.h"

WidgetFind::WidgetFind(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::WidgetFind)
{
    ui->setupUi(this);


    setSignals();

    ui->findEntry->setFocus();
}

WidgetFind::~WidgetFind()
{
    delete ui;
}

void WidgetFind::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

QString  WidgetFind::getFindExpr() const
{
    return ui->findEntry->text();
}

void WidgetFind::onClose()
{
    emit closing();

    QWidget::close();
}

void WidgetFind::onChangedFindText(const QString &expr)
{
    bool findable = ! expr.isEmpty();

    ui->findNextButton->setEnabled(findable);
    ui->findHLButton->setEnabled(findable);
    ui->findPrevButton->setEnabled(findable);


    QTextDocument::FindFlags searchFlags;

    if(ui->findOptMatchCase->isChecked() ) searchFlags = searchFlags | QTextDocument::FindCaseSensitively;


    emit changedFindText(expr);
}

void WidgetFind::onPressedFindNext()
{
    QString expr = ui->findEntry->text();
    QTextDocument::FindFlags searchFlags;

    if(ui->findOptMatchCase->isChecked() ) searchFlags = searchFlags | QTextDocument::FindCaseSensitively;

    emit find();
    emit find(expr, searchFlags);
}

void WidgetFind::onPressedFindPrev()
{
    QString expr = ui->findEntry->text();
    QTextDocument::FindFlags searchFlags;

    if(ui->findOptMatchCase->isChecked() ) searchFlags = searchFlags | QTextDocument::FindCaseSensitively;

    searchFlags = searchFlags | QTextDocument::FindBackward;

    emit find();
    emit find(expr, searchFlags);
}

void WidgetFind::onToggledHighlight(bool activate)
{
    QString expr = ui->findEntry->text();
    QTextDocument::FindFlags searchFlags;

    if(ui->findOptMatchCase->isChecked() ) searchFlags = searchFlags | QTextDocument::FindCaseSensitively;

    emit toggledHighlight(activate);

    if(activate)
        emit highlightAll(expr, searchFlags);
    else
        emit highlightAll("", searchFlags);
}

void WidgetFind::setFindExpr(const QString &expr)
{
    ui->findEntry->setText(expr);

}

void WidgetFind::setSignals()
{
    connect(ui->findEntry, SIGNAL(textChanged(QString)), this, SLOT(onChangedFindText(QString)) );
    connect(ui->findHLButton, SIGNAL(toggled(bool)), this, SLOT(onToggledHighlight(bool)) );
    connect(ui->findNextButton, SIGNAL(released()), this, SLOT(onPressedFindNext()) );
    connect(ui->findPrevButton, SIGNAL(released()), this, SLOT(onPressedFindPrev()) );

    connect(ui->findCloseButton, SIGNAL(clicked()), this, SLOT(onClose()) );

}

void WidgetFind::show()
{
    QWidget::show();

    ui->findEntry->setFocus();
    ui->findHLButton->setChecked(false);

}
