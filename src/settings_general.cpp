#include <QSettings>

#include "equalx_exception.h"
#include "settings_general.h"

namespace  {
//---------------------------------------------------------------------------------------
// Factory Default Settings
//---------------------------------------------------------------------------------------
const bool  DEFAULT_REMEMBER_LAYOUT = true;
const int   DEFAULT_WINDOW_WIDTH    = 800;
const int   DEFAULT_WINDOW_HEIGHT   = 800;
const int   DEFAULT_WINDOW_X        = 200;
const int   DEFAULT_WINDOW_Y        = 200;
const bool  DEFAULT_SHOW_TOOLBAR    = true;
const bool  DEFAULT_SHOW_MAIN_TOOLBAR   = true;
const bool  DEFAULT_SHOW_PANEL_SYMBOLS  = true;
const bool  DEFAULT_SHOW_PANEL_TEMPLATES= true;
const bool  DEFAULT_SHOW_PANEL_PROPERTIES=true;
const bool  DEFAULT_SHOW_PANEL_SIDEBAR  = false;
const char  DEFAULT_EXPORT_TYPE[] = "SVG";

} // end ns anon


equalx::SettingsGeneral::SettingsGeneral()
    : m_rememberLayout(DEFAULT_REMEMBER_LAYOUT),
      m_isVisibleMainBar( DEFAULT_SHOW_MAIN_TOOLBAR ),
      m_isVisibleProps( DEFAULT_SHOW_PANEL_PROPERTIES ),
      m_isVisibleSymbols( DEFAULT_SHOW_PANEL_SYMBOLS ),
      m_isVisibleTemps( DEFAULT_SHOW_PANEL_TEMPLATES ),
      m_isVisibleLib( DEFAULT_SHOW_PANEL_SIDEBAR ),
      m_versionTag( GIT_TAG_VERSION ),
      m_pos( DEFAULT_WINDOW_X, DEFAULT_WINDOW_Y ),
      m_size( DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT ),
      m_exportTypeDefault( DEFAULT_EXPORT_TYPE )
{

}

equalx::SettingsGeneral::SettingsGeneral(QSettings &file)
{
    load(file);
}

bool equalx::SettingsGeneral::isVisibleProps() const
{
    return m_isVisibleProps;
}

void equalx::SettingsGeneral::setIsVisibleProps(bool isVisibleProps)
{
    m_isVisibleProps = isVisibleProps;
}

QString equalx::SettingsGeneral::versionTag() const
{
    return m_versionTag;
}

void equalx::SettingsGeneral::setVersionTag(const QString &versionTag)
{
    m_versionTag = versionTag;
}

bool equalx::SettingsGeneral::rememberLayout() const
{
    return m_rememberLayout;
}

void equalx::SettingsGeneral::setRememberLayout(bool rememberLayout)
{
    m_rememberLayout = rememberLayout;
}

QPoint equalx::SettingsGeneral::pos() const
{
    return m_pos;
}

void equalx::SettingsGeneral::setPos(const QPoint &pos)
{
    m_pos = pos;
}

QSize equalx::SettingsGeneral::size() const
{
    return m_size;
}

void equalx::SettingsGeneral::setSize(const QSize &size)
{
    m_size = size;
}

QString equalx::SettingsGeneral::exportTypeDefault() const
{
    return m_exportTypeDefault;
}

void equalx::SettingsGeneral::setExportTypeDefault(const QString &exportTypeDefault)
{
    m_exportTypeDefault = exportTypeDefault;
}

bool equalx::SettingsGeneral::isVisibleMainBar() const
{
    return m_isVisibleMainBar;
}

void equalx::SettingsGeneral::setIsVisibleMainBar(bool isVisibleMainBar)
{
    m_isVisibleMainBar = isVisibleMainBar;
}

bool equalx::SettingsGeneral::isVisibleSymbols() const
{
    return m_isVisibleSymbols;
}

void equalx::SettingsGeneral::setIsVisibleSymbols(bool isVisibleSymbols)
{
    m_isVisibleSymbols = isVisibleSymbols;
}

bool equalx::SettingsGeneral::isVisibleTemps() const
{
    return m_isVisibleTemps;
}

void equalx::SettingsGeneral::setIsVisibleTemps(bool isVisibleTemps)
{
    m_isVisibleTemps = isVisibleTemps;
}

bool equalx::SettingsGeneral::isVisibleLib() const
{
    return m_isVisibleLib;
}

void equalx::SettingsGeneral::setIsVisibleLib(bool isVisibleLib)
{
    m_isVisibleLib = isVisibleLib;
}

void equalx::SettingsGeneral::load(QSettings &file)
{
    file.beginGroup(name());
    m_versionTag = file.value("version", GIT_TAG_VERSION).toString();
    m_rememberLayout = file.value("remember_layout", DEFAULT_REMEMBER_LAYOUT).toBool();
    if(m_rememberLayout){ //if set to remember layout
        // we read settings saved on the disk
        m_pos = file.value("position", QPoint(DEFAULT_WINDOW_X, DEFAULT_WINDOW_Y)).toPoint();
        m_size= file.value("size", QSize(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT)).toSize();

        m_isVisibleMainBar = file.value("mainbar", DEFAULT_SHOW_MAIN_TOOLBAR).toBool();
        m_isVisibleProps = file.value("panel_properties", DEFAULT_SHOW_PANEL_PROPERTIES).toBool();
        m_isVisibleSymbols = file.value("panel_symbols", DEFAULT_SHOW_PANEL_SYMBOLS).toBool();
        m_isVisibleTemps = file.value("panel_templates", DEFAULT_SHOW_PANEL_TEMPLATES).toBool();
        m_isVisibleLib = file.value("panel_sidebar", DEFAULT_SHOW_PANEL_SIDEBAR).toBool();
    }
    else { // not set to remember layout
        // we use default settings, regardless of whats saved on disk
        m_pos = QPoint(DEFAULT_WINDOW_X, DEFAULT_WINDOW_Y);
        m_size= QSize(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT);

        m_isVisibleMainBar = DEFAULT_SHOW_MAIN_TOOLBAR;
        m_isVisibleProps = DEFAULT_SHOW_PANEL_PROPERTIES;
        m_isVisibleSymbols = DEFAULT_SHOW_PANEL_SYMBOLS;
        m_isVisibleTemps = DEFAULT_SHOW_PANEL_TEMPLATES;
        m_isVisibleLib = DEFAULT_SHOW_PANEL_SIDEBAR;
    }

    m_exportTypeDefault = file.value("export_type", DEFAULT_EXPORT_TYPE).toString() ;

    file.endGroup();
}

void equalx::SettingsGeneral::save(QSettings &settings)
{
    if(!settings.isWritable()) {
        throw equalx::Exception("SettingsGeneral::save", "Settings file is not writable");
    }

    settings.beginGroup(name());
    settings.setValue("remember_layout", m_rememberLayout);
    settings.setValue("position", m_pos );
    settings.setValue("size", m_size);
    settings.setValue("mainbar", m_isVisibleMainBar );
    settings.setValue("panel_properties", m_isVisibleProps );
    settings.setValue("panel_symbols", m_isVisibleSymbols );
    settings.setValue("panel_templates", m_isVisibleTemps );
    settings.setValue("panel_sidebar", m_isVisibleLib );
    settings.setValue("export_type", m_exportTypeDefault );
    settings.endGroup();

    settings.sync();

    if(settings.status() != QSettings::NoError){
        throw equalx::Exception("SettingsGeneral::save", "Error occured while trying to save settings");
    }
}

void equalx::SettingsGeneral::reset()
{
    m_rememberLayout = DEFAULT_REMEMBER_LAYOUT;
    m_isVisibleMainBar =  DEFAULT_SHOW_MAIN_TOOLBAR ;
    m_isVisibleProps =  DEFAULT_SHOW_PANEL_PROPERTIES ;
    m_isVisibleSymbols =  DEFAULT_SHOW_PANEL_SYMBOLS ;
    m_isVisibleTemps =  DEFAULT_SHOW_PANEL_TEMPLATES ;
    m_isVisibleLib =  DEFAULT_SHOW_PANEL_SIDEBAR ;
    m_versionTag =  GIT_TAG_VERSION ;
    m_pos.setX(DEFAULT_WINDOW_X);
    m_pos.setY(DEFAULT_WINDOW_Y);
    m_size.setWidth(DEFAULT_WINDOW_WIDTH);
    m_size.setHeight(DEFAULT_WINDOW_HEIGHT);
    m_exportTypeDefault =  DEFAULT_EXPORT_TYPE ;
}
