#include <QStandardPaths>

#include "abstract_settings_group.h"

QString equalx::AbstractSettingsGroup::default_storage()
{
// if the packager specified a path during compilation, use that..
#ifdef USER_STORAGE
    return QString(USER_STORAGE)
#else
    //.. otherwise, use the standard location path on the current platform
    // (Linux: ~/.local/share/equalx, Windows: C:/Users/<USER>/AppData/Local/equalx, etc.)
    auto storage_dirs_list = QStandardPaths::standardLocations(QStandardPaths::AppLocalDataLocation);

    return storage_dirs_list.front(); // take the first
#endif
}

QString equalx::AbstractSettingsGroup::default_workspace()
{
    return QStandardPaths::standardLocations(QStandardPaths::TempLocation).first();
}

equalx::AbstractSettingsGroup::~AbstractSettingsGroup()
{

}

