/*
 * Copyright 2013 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>

#include <QColorDialog>
#include <QStandardItemModel>

#include "WidgetColorPicker.h"

WidgetColorPicker::WidgetColorPicker(QWidget *parent)
    : QComboBox(parent),
      mCurrentIndex(1)
{
    setMaxCount(21);

    addItem("Custom Color..");
    addItem(createIcon("transparent"),"transparent",QColor(Qt::transparent));
    addItem(createIcon("#000000"),"black",QColor(Qt::black));
    addItem(createIcon("#FFFFFF"),"white",QColor(Qt::white));
    addItem(createIcon("#FF0000"),"red",QColor(Qt::red));
    addItem(createIcon("#800000"),"dark red",QColor(Qt::darkRed));
    addItem(createIcon("#00ff00"),"green",QColor(Qt::green));
    addItem(createIcon("#008000"),"dark green",QColor(Qt::darkGreen));
    addItem(createIcon("#0000ff"),"blue",QColor(Qt::blue));
    addItem(createIcon("#000080"),"dark blue",QColor(Qt::darkBlue));
    addItem(createIcon("#00ffff"),"cyan",QColor(Qt::cyan));
    addItem(createIcon("#ff00ff"),"magenta",QColor(Qt::magenta));
    addItem(createIcon("#ffff00"),"yellow",QColor(Qt::yellow));
    addItem(createIcon("#a0a0a4"),"gray",QColor(Qt::gray));
    addItem(createIcon("#808080"),"dark gray",QColor(Qt::darkGray));
    addItem(createIcon("#c0c0c0"),"light gray",QColor(Qt::lightGray));

    setCurrentIndex(1);

    connect(this,SIGNAL(currentIndexChanged(int)),this,SLOT(on_comboBox_currentIndexChanged(int)));
}

QIcon WidgetColorPicker::createIcon(const QString &colorName) const
{
    QPixmap iconPixmap(32,32);
    iconPixmap.fill(QColor(colorName));
    QIcon itemIcon(iconPixmap);
    return itemIcon;
}

void WidgetColorPicker::on_comboBox_currentIndexChanged(int index)
{
    if(index==0)
    {
        QColor chosen = QColorDialog::getColor(mColor,this);

        if(!chosen.isValid() || chosen == mColor){
            setCurrentIndex(mCurrentIndex);
        }
        else{
            mColor = chosen;
            mColorName = mColor.name();
            addColor(mColor);
        }
    }
    else {
        mColorName = itemText(index);
        mColor     = itemData(index).value<QColor>();
        mCurrentIndex = index;
    }

    emit colorSelectionChanged(mColorName);
    emit colorSelectionChanged(mColor);
}

void WidgetColorPicker::setCurrentIndex(int index)
{
    mCurrentIndex = index;
    QComboBox::setCurrentIndex(index);
}

void WidgetColorPicker::setCurrentColor(const QString &colorName)
{
    if(!colorName.isEmpty() && QColor(colorName).isValid()){
        mColorName = colorName;
        mColor = QColor(colorName);
        addColor(colorName);
    }
}

void WidgetColorPicker::setCurrentColor(const QColor& color)
{
    if(color.isValid())
    {
        mColorName = color.name();
        mColor = color;

        qDebug() << "Color: " << color;

        if(color.alpha()==0){
            mColorName = "transparent";
            mColor = QColor(Qt::transparent);

            qDebug() << "Color is Transparent!";
        }

        int index;
        if((index = findData(color, Qt::UserRole, Qt::MatchExactly | Qt::MatchCaseSensitive)) > -1)
            setCurrentIndex(index);
        else
            addColor(mColor);
    }
}

QColor WidgetColorPicker::getColor() const
{
    return mColor;
}

QString WidgetColorPicker::getColorName() const
{
    return mColorName;
}

void WidgetColorPicker::addColor(const QColor& color)
{    
    QString colorName = color.name().toUpper();
    insertItem(1,createIcon(colorName),colorName,color);
    setCurrentIndex(1);
}

void WidgetColorPicker::addColor(const QString& colorName)
{
    int index;

    if((index = findText(colorName)) > -1)
        setCurrentIndex(index);
    else
    {
        insertItem(1,createIcon(colorName),colorName,QColor(colorName));
        setCurrentIndex(1);
    }
}
