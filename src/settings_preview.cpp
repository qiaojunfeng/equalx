/*
 * Copyright 2019 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QSettings>

#include "equalx_exception.h"
#include "settings_preview.h"

// declare these in order to READ/WRITE using QVariant with QSettings
Q_DECLARE_METATYPE(equalx::FONT_SIZE)
Q_DECLARE_METATYPE(equalx::ENVIRONMENT_MODE)

namespace  {
//---------------------------------------------------------------------------------------
// Factory Default Settings
//---------------------------------------------------------------------------------------
// PREVIEW defaults
const bool  DEFAULT_UPDATE_AUTO = false;
const int   DEFAULT_UPDATE_TIME = 1500; // update time interval in milliseconds
const char  DEFAULT_PREVIEW_BG[]= "transparent";
const char  DEFAULT_RENDER_FG[] = "black";
const char  DEFAULT_RENDER_BG[] = "transparent";

const equalx::FONT_SIZE DEFAULT_RENDER_FONT_SIZE     = equalx::FONT_SIZE::Normal;
const equalx::ENVIRONMENT_MODE DEFAULT_RENDER_MODE   = equalx::ENVIRONMENT_MODE::Display;
} // end ns anon

equalx::SettingsPreview::SettingsPreview()
    : m_previewBgColor(DEFAULT_PREVIEW_BG),
      m_fgColor(DEFAULT_RENDER_FG),
      m_bgColor(DEFAULT_RENDER_BG),
      m_latexMode(DEFAULT_RENDER_MODE),
      m_fontSize(DEFAULT_RENDER_FONT_SIZE),
      m_updateTime(DEFAULT_UPDATE_TIME),
      m_isAutoUpdate(DEFAULT_UPDATE_AUTO)
{

}

equalx::SettingsPreview::SettingsPreview(QSettings &file)
{
    load(file);
}

QString equalx::SettingsPreview::equationBackgroundColor() const
{
    return m_bgColor;
}

void equalx::SettingsPreview::setEquationBackgroundColor(const QString &BgColor)
{
    m_bgColor = BgColor;
}

int equalx::SettingsPreview::updateTime() const
{
    return m_updateTime;
}

void equalx::SettingsPreview::setUpdateTime(int updateTime)
{
    m_updateTime = updateTime;
}

QString equalx::SettingsPreview::BgColor() const
{
    return m_previewBgColor;
}

void equalx::SettingsPreview::setBgColor(const QString &previewBgColor)
{
    m_previewBgColor = previewBgColor;
}

bool equalx::SettingsPreview::isAutoUpdate() const
{
    return m_isAutoUpdate;
}

void equalx::SettingsPreview::setAutoUpdate(bool isAutoUpdate)
{
    m_isAutoUpdate = isAutoUpdate;
}

QString equalx::SettingsPreview::equationForegroundColor() const
{
    return m_fgColor;
}

void equalx::SettingsPreview::setEquationForegroundColor(const QString &fgColor)
{
    m_fgColor = fgColor;
}

equalx::FONT_SIZE equalx::SettingsPreview::equationFontSize() const
{
    return m_fontSize;
}

void equalx::SettingsPreview::setEquationFontSize(equalx::FONT_SIZE fontSize)
{
    m_fontSize = fontSize;
}

equalx::ENVIRONMENT_MODE equalx::SettingsPreview::environmentMode() const
{
    return m_latexMode;
}

void equalx::SettingsPreview::setEnvironmentMode(ENVIRONMENT_MODE renderMode)
{
    m_latexMode = renderMode;
}

void equalx::SettingsPreview::load(QSettings &settings)
{
    settings.beginGroup(name());
    m_isAutoUpdate = settings.value("updateAuto", DEFAULT_UPDATE_AUTO).toBool();
    m_updateTime =   settings.value("updateTime", DEFAULT_UPDATE_TIME).toInt();
    m_fontSize = static_cast<equalx::FONT_SIZE>(settings.value("render_fontsize", static_cast<int>(DEFAULT_RENDER_FONT_SIZE)).toInt());
    m_fgColor = settings.value("render_fgcolor", DEFAULT_RENDER_FG).toString();
    m_bgColor = settings.value("render_bgcolor", DEFAULT_RENDER_BG).toString();
    m_previewBgColor = settings.value("bgcolor", DEFAULT_PREVIEW_BG).toString();
    m_latexMode = static_cast<equalx::ENVIRONMENT_MODE>(settings.value("render_mode", static_cast<int>(DEFAULT_RENDER_MODE)).toInt());
    settings.endGroup();
}

void equalx::SettingsPreview::save(QSettings &settings)
{
    if(!settings.isWritable()) {
        throw equalx::Exception("SettingsPreview::save", "Settings file is not writable");
    }

    settings.beginGroup(name());
    settings.setValue("updateAuto", m_isAutoUpdate);
    settings.setValue("updateTime", m_updateTime );
    settings.setValue("bgcolor", m_previewBgColor);
    settings.setValue("render_fgcolor", m_fgColor );
    settings.setValue("render_bgcolor", m_bgColor );
    settings.setValue("render_fontsize", static_cast<int>(m_fontSize) );
    settings.setValue("render_mode", static_cast<int>(m_latexMode) );
    settings.endGroup();

    settings.sync();

    if(settings.status() != QSettings::NoError){
        throw equalx::Exception("SettingsPreview::save", "Error occured while trying to save settings");
    }
}

void equalx::SettingsPreview::reset()
{
    m_previewBgColor = DEFAULT_PREVIEW_BG;
    m_fgColor = DEFAULT_RENDER_FG;
    m_bgColor = DEFAULT_RENDER_BG;
    m_latexMode = DEFAULT_RENDER_MODE;
    m_fontSize = DEFAULT_RENDER_FONT_SIZE;
    m_updateTime = DEFAULT_UPDATE_TIME;
    m_isAutoUpdate = DEFAULT_UPDATE_AUTO;
}
