/**
 ** This file is part of the equalx project.
 ** Copyright 2020 Mihai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <QMessageBox>

#include "equalx_exception.h"
#include "preamblefile.h"

#include "preambletool.h"
#include "ui_preambletool.h"

PreambleTool::PreambleTool(QWidget *parent) :
    QWidget(parent, Qt::Tool | Qt::Dialog),
    ui(new Ui::PreambleTool),
    mAppliedPreambleText{}
{
    ui->setupUi(this);

    connect(ui->cb_preamblesList, SIGNAL(activated(int)), this, SLOT(onPreambleSelected(int)));
    connect(ui->pb_reset, SIGNAL(released()), this, SLOT(onReset()));
    connect(ui->pb_apply, SIGNAL(released()), this, SLOT(onApply()));
}

PreambleTool::~PreambleTool()
{
    delete ui;
}

QString PreambleTool::preambleText() const
{
    return mAppliedPreambleText;
}

void PreambleTool::setPreambleText(const QString &text)
{
    mOrigPreambleText = text;
    ui->te_preamble->setPlainText(text);
}

void PreambleTool::setPreamblesList(const std::vector<equalx::SettingsFileData> &preambles)
{
    ui->cb_preamblesList->clear();
    for(const auto& preamble : preambles) {
        ui->cb_preamblesList->addItem(preamble.title, preamble.path);
    }

    ui->cb_preamblesList->setCurrentIndex(-1);
}

void PreambleTool::setEditorHighlighting(bool value)
{
    ui->te_preamble->setHighLighting( value );
}

void PreambleTool::setEditorCompletion(bool value)
{
    ui->te_preamble->setCompletion( value );
}

void PreambleTool::setEditorCompletionCaseSensitive(bool value)
{
    ui->te_preamble->setCompletionSensitive( value );
}

void PreambleTool::setEditorFont(const QFont &font)
{
    ui->te_preamble->setFont( font );
}

void PreambleTool::onPreambleSelected(int index)
{
    const auto& preamble_filepath = ui->cb_preamblesList->currentData().toString();

    if(preamble_filepath.isEmpty()) return;

    loadPreamble(preamble_filepath);
}

void PreambleTool::onReset()
{
    ui->te_preamble->setPlainText(mOrigPreambleText);

    emit reset();
}

void PreambleTool::onApply()
{
    mAppliedPreambleText = ui->te_preamble->toPlainText();

    emit apply();
}

void PreambleTool::loadPreamble(const QString &path)
{
    // load preamble file
    QString preamble_text;
    try {
        preamble_text = equalx::read_preamble(path);
    } catch (const equalx::Exception& e) {
        QMessageBox::critical(this, "Error",
                              "Preamble Tool encountered error:" + e.what() + " at:\n" + e.where(),
                              QMessageBox::Ok);
    }

    ui->te_preamble->setPlainText(preamble_text);
}
