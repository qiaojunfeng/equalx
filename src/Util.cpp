/*
 * Copyright 2013 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>

#include <QApplication>
#include <QDesktopWidget>
#include <QColor>
#include <QDir>
#include <QString>
#include <QSettings>
#include <QFile>

#include "defines.h"
#include "Settings.h"
#include "Util.h"

namespace equalx{

QMap<QString,QString> MIME_TYPES; // extension => mimetype

int Util::getFontResolution(int fontSizePt)
{
    int fontResolution;
    int dpi = QApplication::desktop()->logicalDpiX();

    fontResolution =  int(fontSizePt*dpi*72.27/720);

    return fontResolution;
}

double Util::getScaleFactor(int sliderValue)
{
    double scale=1.0;

    if(sliderValue==0){
        scale = 0.1;
    }
    else{
        scale = sliderValue/10.0;
    }

    return scale;

}

QColor Util::oppositeColor(const QColor& color)
{
    QColor op =color.toHsl();
    int h, s, l;

    op.getHsl(&h,&s,&l);

    h+= 180;
    s=1;
    l=1;
    op.setHsl(h,s,l);
    op.toRgb();

    return op;
}

QString Util::MimeTypeOf(const QString &fileExtension)
{
    return MIME_TYPES.value(fileExtension, "application/octet-stream");
}

void Util::init()
{
    MIME_TYPES.clear();
    MIME_TYPES.insert("eps", "application/postscript");
    MIME_TYPES.insert("jpg", "image/jpeg");
    MIME_TYPES.insert("pdf", "application/pdf");
    MIME_TYPES.insert("png", "image/png");
    MIME_TYPES.insert("ps", "application/postscript");
    MIME_TYPES.insert("svg", "image/svg+xml");
    MIME_TYPES.insert("tex", "text/plain");

}

QMap<QString, QString> &Util::getMimeTypes()
{
    return MIME_TYPES;
}

equalx::ErrorsList Util::parse_latex_errors(const QString& content)
{
    equalx::ErrorsList errorsList;

    if(!has_latex_errors(content)){  // do not try to parse the string for errors when there are none
        return errorsList;
    }

    QString errorMessage;
    int lineNumber;

    int pos1 = content.indexOf("!", 0);
    int pos2 = -1;

    /* this substracts the preamble line numbers
    QStringList preambleStr = mFile.info().preamble().split("\n");
    int preambleLines = preambleStr.count();
    */

    while(pos1>0){
        pos2 = content.indexOf("\n", pos1);

        errorMessage = content.mid(pos1+1, pos2-pos1-1);

        pos1 = content.indexOf("l.", pos2, Qt::CaseSensitive);
        pos2 = content.indexOf(" ", pos1);

        lineNumber = (content.midRef(pos1+2, pos2-pos1-2)).toInt();

        errorsList.insertMulti(lineNumber, errorMessage);

        pos1 = content.indexOf("!", pos1);
    }

    return errorsList;
}

} // end namespace EqualX
