    /**
 ** This file is part of the equalx project.
 ** Copyright 2019 Mihai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <algorithm>

#include <QFileInfo>
#include <QSettings>
#include <QTemporaryFile>

#include "equalx_exception.h"
#include "settings_templates.h"

namespace {
// default factory settings
const char TEMPLATES_DIR[]      = "templates";
const char PREAMBLES_DIR[]      = "preambles";


inline bool file_in_dir(const QString& file_path, const QString& dir_path) {
    QFileInfo fi(file_path);

    return fi.canonicalPath() == dir_path;
}

inline QString copy_file(const QString& file_path, const QString& dir_path) {
    QFileInfo fi(file_path);
    QString new_file_path = dir_path + "/" + fi.fileName();
    QFile::copy(file_path, new_file_path); // copy file to dir_path

    return new_file_path;
}

} // end anon ns

equalx::SettingsTemplates::SettingsTemplates()
    : AbstractSettingsGroup(),
      m_defaultPreamble{0},
      m_defaultBodyTemplate{0}
{

}

equalx::SettingsTemplates::SettingsTemplates(QSettings &file)
    : SettingsTemplates()
{
    load(file);
}

equalx::PreambleFileList::size_type equalx::SettingsTemplates::getDefaultPreambleIndex() const
{
    // range check
    if(m_preambles.size() == 0) {
        return 0;
    }

    return m_defaultPreamble;
}

std::optional<equalx::SettingsFileData> equalx::SettingsTemplates::getDefaultPreamble() const
{
    if(m_defaultPreamble < m_preambles.size()){
        return m_preambles.at(m_defaultPreamble);
    }
    else {
        return {};
    }
}

const equalx::PreambleFileList &equalx::SettingsTemplates::getAllPreambles() const
{
    return m_preambles;
}

void equalx::SettingsTemplates::setDefaultPreambleIndex(PreambleFileList::size_type index)
{
    m_defaultPreamble = index;
}

std::optional<equalx::SettingsFileData> equalx::SettingsTemplates::getPreamble(const QString &path) const
{
    auto iter = std::find_if(m_preambles.cbegin(), m_preambles.cend(), [&path](const auto& e) {
        return e.path == path;
    });

    if(iter == m_preambles.cend()) {
        return {};
    }

    return *iter;
}

std::optional<equalx::SettingsFileData> equalx::SettingsTemplates::getPreamble(equalx::PreambleFileList::size_type index) const
{
    if(index > m_preambles.size())
        return {};

    return m_preambles.at(index);
}

equalx::PreambleFileList::size_type equalx::SettingsTemplates::countPreambles() const
{
    return m_preambles.size();
}

void equalx::SettingsTemplates::setPreambleTitle(const QString &path, const QString &title)
{
    auto iter = std::find_if(m_preambles.begin(), m_preambles.end(), [&path](const auto& e) {
        return e.path == path;
    });

    if(iter != m_preambles.end()) {
        iter->title = title;
    }
}

void equalx::SettingsTemplates::addPreamble(const QString &path, const QString &title)
{
    // generate a unique file name:
    equalx::SettingsFileData file_ptr{title, path};

    m_preambles.push_back(file_ptr);
}

void equalx::SettingsTemplates::removePreamble(const QString &file_path)
{
    if(file_path.isEmpty()) return;

    m_preambles.erase(std::remove_if(m_preambles.begin(), m_preambles.end(), [file_path](const auto& elem)
    {
        return elem.path == file_path;
    }));
}

void equalx::SettingsTemplates::clearPreambles()
{
    m_preambles.clear();
}

equalx::BodyTemplateList::size_type equalx::SettingsTemplates::getDefaultBodyTemplateIndex() const
{
    // range check
    if(m_bodyTemplates.size() == 0) {
        return 0;
    }

    return m_defaultBodyTemplate;
}

std::optional<equalx::SettingsFileData> equalx::SettingsTemplates::getDefaultBodyTemplate() const
{
    if(m_defaultBodyTemplate < m_bodyTemplates.size()){
        return m_bodyTemplates.at(m_defaultBodyTemplate);
    }
    else {
        return {};
    }
}

const equalx::BodyTemplateList &equalx::SettingsTemplates::getAllBodyTemplates() const
{
    return m_bodyTemplates;
}

void equalx::SettingsTemplates::setDefaultPreambleBodyTemplateIndex(BodyTemplateList::size_type index)
{
    m_defaultBodyTemplate = index;
}

void equalx::SettingsTemplates::addBodyTemplate(const QString &path, const QString &title)
{

    equalx::SettingsFileData file_ptr{title, path};

    m_bodyTemplates.push_back(file_ptr);
}

std::optional<equalx::SettingsFileData> equalx::SettingsTemplates::getBodyTemplate(const QString &path) const
{
    auto iter = std::find_if(m_bodyTemplates.cbegin(), m_bodyTemplates.cend(), [&path](const auto& e) {
        return e.path == path;
    });

    if(iter == m_bodyTemplates.end()) {
        return {};
    }

    return *iter;
}

std::optional<equalx::SettingsFileData> equalx::SettingsTemplates::getBodyTemplate(equalx::BodyTemplateList::size_type index) const
{
    if(index < m_bodyTemplates.size())
        return {};
    else {
        return m_bodyTemplates.at(index);
    }
}

equalx::BodyTemplateList::size_type equalx::SettingsTemplates::countBodyTemplates() const
{
    return m_bodyTemplates.size();
}

void equalx::SettingsTemplates::setBodyTemplateTitle(const QString &path, const QString &title)
{
    auto iter = std::find_if(m_bodyTemplates.begin(), m_bodyTemplates.end(), [&path](const auto& e) {
        return e.path == path;
    });

    if(iter != m_bodyTemplates.end()) {
        iter->title = title;
    }
}

void equalx::SettingsTemplates::removeBodyTemplate(const QString &file_path)
{
    if(file_path.isEmpty()) return;

    m_bodyTemplates.erase(std::remove_if(m_bodyTemplates.begin(), m_bodyTemplates.end(), [&file_path](const auto& elem)
    {
        return elem.path == file_path;
    }));
}

void equalx::SettingsTemplates::clearBodyTemplate()
{
    m_bodyTemplates.clear();
}

QString equalx::SettingsTemplates::storage_preambles() const
{
    return default_storage() + "/" + PREAMBLES_DIR;
}

QString equalx::SettingsTemplates::storage_templates() const
{
    return default_storage() + "/" + TEMPLATES_DIR;
}

void equalx::SettingsTemplates::load(QSettings &settings)
{
    // clear current lists..
    reset();

    settings.beginGroup(name());
    //--- preambles templates
    std::size_t sizePreambles = static_cast<std::size_t>(settings.beginReadArray("preambles"));
    for (std::size_t i = 0; i < sizePreambles; ++i) {
        settings.setArrayIndex(static_cast<int>(i));
        SettingsFileData file_ptr;
        file_ptr.title  = settings.value("title").toString();
        file_ptr.path   = settings.value("path").toString();

        if(!file_ptr.path.isEmpty()){ // we only add files that have a real path
            m_preambles.push_back(file_ptr);
        }
    }
    settings.endArray();
    m_defaultPreamble = settings.value("default_preamble", -1).toUInt();

    // sanity check for default preamble
    if(m_preambles.size() == 0 || m_defaultPreamble > m_preambles.size()) { // if default preamble is not set or is grater than the list
        m_defaultPreamble = 0;
    }

    //--- body templates
    decltype (m_defaultBodyTemplate) sizeEnvironments = static_cast<decltype (m_defaultBodyTemplate)>(settings.beginReadArray("bodytemplates"));
    for (std::size_t i = 0; i < sizeEnvironments; ++i) {
        settings.setArrayIndex(static_cast<int>(i));
        SettingsFileData file_ptr;
        file_ptr.title  = settings.value("title").toString();
        file_ptr.path   = settings.value("path").toString();

        if(!file_ptr.path.isEmpty()){ // we only add files that have a real path
            m_bodyTemplates.push_back(file_ptr);
        }
    }
    settings.endArray();
    m_defaultBodyTemplate = settings.value("default_bodytemplate", -1).toUInt();

    // sanity check for default preamble
    if(m_bodyTemplates.size() == 0 || m_defaultBodyTemplate > m_bodyTemplates.size()) {// if default preamble is not set or is grater than the list
        m_defaultBodyTemplate = 0;
    }

    settings.endGroup();
}

void equalx::SettingsTemplates::save(QSettings &settings)
{
    if(!settings.isWritable()) {
        throw equalx::Exception("SettingsTemplates::save", "Settings file is not writable");
    }

    settings.beginGroup(name());
    settings.setValue("default_preamble", QVariant::fromValue(m_defaultPreamble));
    settings.beginWriteArray("preambles");
    for (std::size_t i = 0; i < m_preambles.size(); ++i) {
        settings.setArrayIndex(static_cast<int>(i));

        auto &preamble_data = m_preambles.at(i);
        // check if file is located in the preambles storage directory..
        if(!file_in_dir(preamble_data.path, storage_preambles())) {
            // if not, then copy it in the preambles directory
            preamble_data.path = copy_file(preamble_data.path, storage_preambles());
        }

        settings.setValue("title", preamble_data.title);
        settings.setValue("path", preamble_data.path);
    }
    settings.endArray();

    settings.setValue("default_bodytemplate", QVariant::fromValue(m_defaultBodyTemplate));
    settings.beginWriteArray("bodytemplates");
    for (std::size_t i = 0; i < m_bodyTemplates.size(); ++i) {
        settings.setArrayIndex(static_cast<int>(i));

        auto &bodyTemplate_data = m_bodyTemplates.at(i);
        // check if file is located in the bodyTemplate storage directory..
        if(!file_in_dir(bodyTemplate_data.path, storage_preambles())) {
            // if not, then copy it in the bodyTemplate directory
            bodyTemplate_data.path = copy_file(bodyTemplate_data.path, storage_templates());
        }

        settings.setValue("title", bodyTemplate_data.title);
        settings.setValue("path", bodyTemplate_data.path);
    }
    settings.endArray();
    settings.endGroup();

    settings.sync();

    if(settings.status() != QSettings::NoError) {
        throw equalx::Exception("SettingsTemplates::save", "Error occured while trying to save settings");
    }
}

void equalx::SettingsTemplates::reset()
{
    m_defaultPreamble = 0;
    m_defaultBodyTemplate = 0;

    // prepare current preambles and templates to be deleted from disk
     for(auto& p : m_preambles) {
        removePreamble(p.path);
    }

    for(auto& b : m_bodyTemplates) {
        removeBodyTemplate(b.path);
    }

    // now clear the lists
    m_preambles.clear();
    m_bodyTemplates.clear();

}

