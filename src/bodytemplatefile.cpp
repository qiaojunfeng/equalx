/**
 ** This file is part of the equalx project.
 ** Copyright 2019 Mihai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/


#include <QByteArray>
#include <QFile>

#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonObject>
#include "equalx_exception.h"
#include "bodytemplatefile.h"

equalx::BodyTemplateFile::BodyTemplateFile()
    : BodyTemplateFile("untitled", "")
{

}

equalx::BodyTemplateFile::BodyTemplateFile(const QString &path)
    : BodyTemplateFile("untitled", path)
{

}

equalx::BodyTemplateFile::BodyTemplateFile(const QString &title, const QString &path)
    : m_isDirty{true},
      m_title{title},
      m_path{path},
      m_beginEnv{"% write here the begining of the body template\n\\begin{eqnarray*}"},
      m_endEnv{"% write here the end of body template\n\\end{eqnarray*}"}
{

}

equalx::BodyTemplateFile::~BodyTemplateFile()
{
}

void equalx::BodyTemplateFile::create(const QString &path)
{
    QFile file(path);
    if(!file.open(QIODevice::NewOnly)) {
        throw equalx::Exception("[BodyTemplateFile::create]", "Couldn't create file.");
    }
}

bool equalx::BodyTemplateFile::read()
{
    QFile file(path());
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        throw equalx::Exception("[BodyTemplateFile::readFile]", QString("Can not Read File:") + path());
    }

    QByteArray file_contents = file.readAll();
    if(file_contents.isEmpty()) return false;

    QJsonParseError error;
    QJsonDocument json_file = QJsonDocument::fromJson(file_contents, &error);
    if(json_file.isNull()) {
        throw equalx::Exception("[BodyTemplateFile::readFile]", error.errorString());
    }

    QJsonObject rootObj = json_file.object();
    if(rootObj.isEmpty()) {
        throw equalx::Exception("[BodyTemplateFile::readFile]", QString("Invalid file format") + error.errorString());
    }

    if(rootObj.contains("title") && rootObj["title"].isString()){
        m_title = rootObj["title"].toString();
    }
    else {
        throw equalx::Exception("[BodyTemplateFile::readFile]", "Invalid file format - missing title of file");
    }

    if(rootObj.contains("beginEnv") && rootObj["beginEnv"].isString()){
        m_beginEnv = rootObj["beginEnv"].toString();
    }
    else {
        throw equalx::Exception("[BodyTemplateFile::readFile]", "Invalid file format - missing begin environment");
    }

    if(rootObj.contains("endEnv") && rootObj["endEnv"].isString()){
        m_endEnv = rootObj["endEnv"].toString();
    }
    else {
        throw equalx::Exception("[BodyTemplateFile::readFile]", "Invalid file format - missing end environment");
    }

    m_isDirty = false;

    return true;
}

void equalx::BodyTemplateFile::update()
{
    if(!m_isDirty) return;

    QFile saveFile(path());
    if (!saveFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
        throw equalx::Exception("[BodyTemplateFile::update]", QString("Can not Write to File:") + path());
    }

    QJsonObject rootObject;
    rootObject["title"]     = m_title;
    rootObject["beginEnv"]  = m_beginEnv;
    rootObject["endEnv"]    = m_endEnv;

    QJsonDocument saveDoc(rootObject);

    if(saveFile.write(saveDoc.toJson()) == -1) { // an error occured
        throw equalx::Exception("[BodyTemplateFile::update]", saveFile.errorString());
    }

    m_isDirty = false;
}

void equalx::BodyTemplateFile::remove()
{
    if(!QFile::remove(path())){
        throw equalx::Exception("[BodyTemplateFile::remove]", "Couldn't remove file: " + path());
    }
}

void equalx::BodyTemplateFile::close()
{
    update();

    m_title.clear();
    m_beginEnv.clear();
    m_endEnv.clear();
}

void equalx::BodyTemplateFile::clear()
{
    m_title.clear();
    m_beginEnv.clear();
    m_endEnv.clear();
}

