/**
 ** This file is part of the equalx project.
 ** Copyright 2019 Mihai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "equalx_exception.h"

equalx::Exception::Exception(const QString &func)
    : m_loc(func),
      m_mess("Unknown")
{

}

equalx::Exception::Exception(const QString &func, const QString &message)
    : m_loc(func),
      m_mess(message)
{

}

equalx::Exception::~Exception()
{

}

QString equalx::Exception::where() const
{
    return m_loc;
}

QString equalx::Exception::what() const
{
    return m_mess;
}

