/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QVariant>
#include "BookmarksPanel/BookmarkItem.h"
#include "BookmarksPanel/BookmarksItemModel.h"

BookmarkItem::BookmarkItem(BookmarkItem *parent)
    : mModel{nullptr},
      mParent(parent)
{
    if(mParent)
        mModel = mParent->model();
}

BookmarkItem::BookmarkItem(const LibraryModelData &data, BookmarkItem *parent)
    : mModel{nullptr},
      mData(data),
      mParent(parent)
{
    if(mParent)
        mModel = mParent->model();
}

BookmarkItem::~BookmarkItem()
{
    qDeleteAll(mChildren);
}

void BookmarkItem::appendChild(BookmarkItem *child)
{
    mChildren.append(child);
}

void BookmarkItem::insertChild(int row, BookmarkItem *child)
{
    mChildren.insert(row, child);
}

BookmarkItem *BookmarkItem::child(int row)
{
    int minSize = 0;
    int maxSize = mChildren.size()-1;

    if(row<minSize) row = minSize;
    if(row>maxSize) row = maxSize;

    return mChildren.value(row);
}

BookmarkItem *BookmarkItem::constChild(int row) const
{
    return mChildren.value(row);
}

void BookmarkItem::clear()
{
    if(!mChildren.isEmpty()){
        qDeleteAll(mChildren);
        mChildren.clear();
    }
}

int BookmarkItem::childCount() const
{
    return mChildren.size();
}

int BookmarkItem::columnCount() const
{
    return 1;
}

LibraryModelData BookmarkItem::data(int /*column*/) const
{
    return mData;
}

void BookmarkItem::setData(const LibraryModelData &data)
{
    mData.id = data.id;
    mData.name = data.name;
    mData.filePath = data.filePath;
    mData.description = data.description;
    mData.type = data.type;
    mData.created = data.created;
}

QModelIndex BookmarkItem::index() const
{
    if(mParent==nullptr)
        return {};

    return mModel->index(row(),0, mParent->index());
}

bool BookmarkItem::hasChildren() const
{
    return (!mChildren.isEmpty());
}

int BookmarkItem::row() const
{
    if(mParent){
        return mParent->mChildren.indexOf(const_cast<BookmarkItem*>(this));
    }

    return 0;
}

int BookmarkItem::rowCount() const
{
    return childCount();
}

BookmarkItem *BookmarkItem::parent() const
{
    return mParent;
}

BookmarksItemModel *BookmarkItem::model() const
{
    return mModel;
}

void BookmarkItem::removeChild(int row)
{
    int minSize = 0;
    int maxSize = mChildren.size()-1;

    if(row<minSize) row = minSize;
    if(row>maxSize) row = maxSize;

    mChildren.removeAt(row);
}

void BookmarkItem::setChildren(const QList<BookmarkItem *>& childList)
{
    mChildren = childList;
}

