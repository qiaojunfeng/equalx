/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "LatexHighlighter.h"

LatexHighlighter::LatexHighlighter(QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{
    HighlightingRule rule;

    commentFormat.setForeground(Qt::gray);
    commentFormat.setFontWeight(QFont::Normal);
    rule.pattern = QRegExp("%.*");
    rule.format = commentFormat;
    rules.append(rule);

    controlFormat.setForeground(Qt::blue);
    controlFormat.setFontWeight(QFont::Bold);
    rule.pattern = QRegExp("\\\\[A-Za-z]*");
    rule.format = controlFormat;
    rules.append(rule);

    bracesFormat.setForeground(Qt::red);
    bracesFormat.setFontWeight(QFont::Normal);
    rule.pattern = QRegExp(R"([\{\}\(\)\[\]\^\_])");
    rule.format = bracesFormat;
    rules.append(rule);

}

void LatexHighlighter::highlightBlock(const QString &text)
{
    foreach (HighlightingRule rule, rules) {
        QRegExp expression(rule.pattern);
        int index = text.indexOf(expression);
        while (index >= 0) {
            int length = expression.matchedLength();
            setFormat(index, length, rule.format);
            index = text.indexOf(expression, index + length);
        }
    }

    setCurrentBlockState(0);

}
