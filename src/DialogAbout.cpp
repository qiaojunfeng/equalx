/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QPropertyAnimation>
#include <QFile>
#include <QTextStream>

#include "DialogAbout.h"

#include "defines.h"

DialogAbout::DialogAbout(QWidget *parent)
    : QDialog(parent)
{
    setupUi(this);

    QString appName = labelAppName->text();
    labelAppName->setText(appName.arg(APP_NAME));

    QString appFullName = labelAppFullName->text();
    labelAppFullName->setText(appFullName.arg(APP_FULL_NAME));

    QString appVersion = labelAppVersion->text();
    labelAppVersion->setText(appVersion.arg(GIT_TAG_VERSION));

    connect(thanksButton, SIGNAL(toggled(bool)), this, SLOT(onClickedThanksButton(bool)) );

    textCredits->setAcceptRichText(true);

    adjustSize();
}

void DialogAbout::onClickedThanksButton(bool show)
{
    int index;

    show?index=1:index=0;
    stackedWidget->setCurrentIndex(index);

    if(show){
        QString thanksText;

        QFile file(":/THANKS");
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            thanksText = stream.readAll();
            stream.flush();
            file.close();

            textCredits->setHtml(thanksText);
        }



        adjustSize();
    }
}
