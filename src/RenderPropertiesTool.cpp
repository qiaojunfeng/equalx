#include <QIntValidator>

#include "RenderPropertiesTool.h"
#include "ui_RenderPropertiesTool.h"

RenderPropertiesTool::RenderPropertiesTool(QWidget *parent) :
    QWidget(parent, Qt::Tool),
    ui(new Ui::ToolWidgetRenderProperties),
    mOrigDefaultType{"svg"}
{
    ui->setupUi(this);

    setWindowFlags(Qt::Tool);

    auto* intVal = new QIntValidator(this);
    ui->resolutionXEdit->setValidator(intVal);
    ui->resolutionYEdit->setValidator(intVal);

    // Setup Signals
    connect(ui->cb_renderConfiguration, SIGNAL(currentIndexChanged(int)), this, SLOT(onConfigurationSelected(int)));
    connect(ui->pb_reset, SIGNAL(released()), this, SLOT(onReset()) );
    connect(ui->pb_apply, SIGNAL(released()), this, SIGNAL(apply()));

    connect(ui->resolutionXEdit, &QLineEdit::textChanged, [this](const QString& text) {
        auto& conf = currentConfiguration();

        conf.options.xres = text.toInt();
    });

    connect(ui->resolutionYEdit, &QLineEdit::textChanged, [this](const QString& text) {
        auto& conf = currentConfiguration();

        conf.options.yres = text.toInt();
    });

    connect(ui->exportTypesComboBox, &QComboBox::currentTextChanged, [this](const QString& text) {
        mOrigDefaultType = text;
    });


    connect(ui->sp_paddingLeft, qOverload<int>(&QSpinBox::valueChanged), [this](int val) {
        auto& conf = currentConfiguration();

        conf.options.padding.setLeft(val);
    });

    connect(ui->sp_paddingTop, qOverload<int>(&QSpinBox::valueChanged), [this](int val) {
        auto& conf = currentConfiguration();

        conf.options.padding.setTop(val);
    });

    connect(ui->sp_paddingRight, qOverload<int>(&QSpinBox::valueChanged), [this](int val) {
        auto& conf = currentConfiguration();

        conf.options.padding.setRight(val);
    });

    connect(ui->sp_paddingBottom, qOverload<int>(&QSpinBox::valueChanged), [this](int val) {
        auto& conf = currentConfiguration();

        conf.options.padding.setBottom(val);
    });

}

RenderPropertiesTool::~RenderPropertiesTool()
{
    delete ui;
}

void RenderPropertiesTool::setConfigurations(const std::vector<equalx::RenderEngineConfiguration> &list)
{
    mOrigConfList    = list;
    mCurrentConfList = list;

    ui->cb_renderConfiguration->clear();
    for(uint i=0; i<static_cast<uint>(list.size()); ++i) {
        ui->cb_renderConfiguration->addItem(list[i].name, i);
    }
}

void RenderPropertiesTool::setDefaultConfiguration(std::vector<equalx::RenderEngineConfiguration>::size_type pos)
{
    mOrigDefaultConf = pos;

    ui->cb_renderConfiguration->setCurrentIndex(pos);
}

void RenderPropertiesTool::setExportTypes(const QStringList &list)
{
    ui->exportTypesComboBox->clear();
    ui->exportTypesComboBox->addItems(list);
}

void RenderPropertiesTool::setDefaultType(const QString &type)
{
    mOrigDefaultType = type;
    ui->exportTypesComboBox->setCurrentText(type);
}

int RenderPropertiesTool::getDefaultConfigurationIndex() const
{
    return ui->cb_renderConfiguration->currentIndex();
}

QString RenderPropertiesTool::getExportType() const
{
    return ui->exportTypesComboBox->currentText();
}

int RenderPropertiesTool::getResolutionX() const
{
    return ui->resolutionXEdit->text().toInt();
}

int RenderPropertiesTool::getResolutionY() const
{
    return ui->resolutionYEdit->text().toInt();
}

void RenderPropertiesTool::getResolution(int &rx, int &ry)
{
    rx = ui->resolutionXEdit->text().toInt();
    ry = ui->resolutionYEdit->text().toInt();
}

QMargins RenderPropertiesTool::getPaddings() const
{
    QMargins margins(ui->sp_paddingLeft->value(), ui->sp_paddingTop->value(), ui->sp_paddingRight->value(), ui->sp_paddingBottom->value());

    return margins;
}

void RenderPropertiesTool::onReset()
{
    setConfigurations(mOrigConfList);

    emit reset();
}

equalx::RenderEngineConfiguration &RenderPropertiesTool::currentConfiguration()
{
    return mCurrentConfList[ui->cb_renderConfiguration->currentData().toInt()];
}

void RenderPropertiesTool::onConfigurationSelected(int index)
{
    if(index == -1 || index>static_cast<int>(mCurrentConfList.size()) ) return;

    int i = index;

    const auto& conf = mCurrentConfList[i];

    setResolution(conf.options.xres, conf.options.yres);
    loadMargins(conf.options.padding);

    // no need to emit settingsChanged() because its emitted by the modifications of the widgets
}

void RenderPropertiesTool::loadMargins(const QMargins &margins)
{
    ui->sp_paddingLeft->setValue(margins.left());
    ui->sp_paddingRight->setValue(margins.right());
    ui->sp_paddingTop->setValue(margins.top());
    ui->sp_paddingBottom->setValue(margins.bottom());
}

void RenderPropertiesTool::selectType(const QString &type)
{
    ui->exportTypesComboBox->setCurrentText(type);
}

void RenderPropertiesTool::setResolution(int rx, int ry)
{
    ui->resolutionXEdit->setText(QString::number(rx));
    ui->resolutionYEdit->setText(QString::number(ry));
}
