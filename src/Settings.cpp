#include <utility> // std::move

#include <QDir>
#include <QSettings>

#include "defines.h"
#include "settings_general.h"
#include "settings_editor.h"
#include "settings_preview.h"
#include "settings_templates.h"
#include "settings_advanced.h"

#include "Settings.h"

namespace {
const QString SETTINGS_ORGANISATION("qiukahnsoft");
const QString SETTINGS_APPLICATION("equalx");
}

equalx::Settings::Settings()
    : mOrganisation(SETTINGS_ORGANISATION),
      mApp(SETTINGS_APPLICATION),
      mGeneral(new SettingsGeneral),
      mEditor(new SettingsEditor),
      mPreview(new SettingsPreview),
      mTemplates(new SettingsTemplates),
      mAdvanced(new SettingsAdvanced)
{

}

equalx::Settings::Settings(equalx::Settings const& other)
    : mOrganisation(other.mOrganisation),
      mApp(other.mApp),
      mGeneral(new SettingsGeneral(*other.mGeneral)),
      mEditor(new SettingsEditor(*other.mEditor)),
      mPreview(new SettingsPreview(*other.mPreview)),
      mTemplates(new SettingsTemplates(*other.mTemplates)),
      mAdvanced(new SettingsAdvanced(*other.mAdvanced))
{

}

equalx::Settings::Settings(equalx::Settings &&other) noexcept
    : mOrganisation(std::move(other.mOrganisation)),
      mApp(std::move(other.mApp)),
      mGeneral(std::move(other.mGeneral)),
      mEditor(std::move(other.mEditor)),
      mPreview(std::move(other.mPreview)),
      mTemplates(std::move(other.mTemplates)),
      mAdvanced(std::move(other.mAdvanced))
{

}

equalx::Settings& equalx::Settings::operator=(const equalx::Settings &other)
{
    if(this!=&other){
        mOrganisation = other.mOrganisation;
        mApp        = other.mApp;
        mGeneral.reset(new SettingsGeneral(*other.mGeneral));
        mEditor.reset(new SettingsEditor(*other.mEditor));
        mPreview.reset(new SettingsPreview(*other.mPreview));
        mTemplates.reset(new SettingsTemplates(*other.mTemplates));
        mAdvanced.reset(new SettingsAdvanced(*other.mAdvanced));
    }

    return *this;
}

equalx::Settings& equalx::Settings::operator=(equalx::Settings &&other) noexcept
{
    if(this!=&other){
        // first clear current settings objects
        mGeneral.reset(nullptr);
        mEditor.reset(nullptr);
        mPreview.reset(nullptr);
        mTemplates.reset(nullptr);
        mAdvanced.reset(nullptr);

        // swap null to the others
        mGeneral.swap(other.mGeneral);
        mEditor.swap(other.mEditor);
        mPreview.swap(other.mPreview);
        mTemplates.swap(other.mTemplates);
        mAdvanced.swap(other.mAdvanced);
    }

    return *this;
}

void equalx::Settings::load()
{
    QSettings settings(mOrganisation, mApp);

    // General
    mGeneral->load(settings);

    // Editor
    mEditor->load(settings);

    // Preview
    mPreview->load(settings);

    // Templates
    mTemplates->load(settings);

    // Advanced
    mAdvanced->load(settings);
}

void equalx::Settings::save()
{
    QSettings settings(mOrganisation, mApp);

    mGeneral->save(settings);
    mEditor->save(settings);
    mPreview->save(settings);
    mTemplates->save(settings);
    mAdvanced->save(settings);
}

void equalx::Settings::saveGeneral()
{
    QSettings settings(mOrganisation, mApp);

    mGeneral->save(settings);
}

void equalx::Settings::saveEditor()
{
    QSettings settings(mOrganisation, mApp);

    mEditor->save(settings);
}

void equalx::Settings::savePreview()
{
    QSettings settings(mOrganisation, mApp);

    mPreview->save(settings);
}

void equalx::Settings::saveTemplates()
{
    QSettings settings(mOrganisation, mApp);

    mTemplates->save(settings);
}

void equalx::Settings::saveAdvanced()
{
    QSettings settings(mOrganisation, mApp);

    mAdvanced->save(settings);
}

void equalx::Settings::reset()
{
    // General
    mGeneral->reset();

    // Editor
    mEditor->reset();

    // Preview
    mPreview->reset();

    // Templates
    mTemplates->reset();

    // Advanced Settings
    mAdvanced->reset();
}

void equalx::Settings::setup_storage()
{
    QString dir_storage_path = general()->default_storage();
    QDir dir_storage(dir_storage_path);

    dir_storage.mkpath(dir_storage_path);

    // create sub-directories
    dir_storage.mkpath(templates()->storage_preambles());
    dir_storage.mkpath(templates()->storage_templates());

    dir_storage.mkpath(advanced()->storage_bookmarks());
    dir_storage.mkpath(advanced()->storage_toolbar());
    dir_storage.mkpath(advanced()->storage_history());


}



