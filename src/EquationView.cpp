/*
 * Copyright 2010-2020 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QVBoxLayout>

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>


#include <poppler-qt5.h>

#include "equalx_exception.h"
#include "RenderEngine.h"

#include "EquationView.h"


EquationViewer::EquationViewer(QWidget * parent)
    :  QWidget(parent),
      m_view{new QGraphicsView{this}},
      m_scene{new QGraphicsScene{this}},
      m_item{nullptr},
      m_filepath{},
      m_opts{},
      m_curZoom{1.0f},
      m_isDirty{false},
      m_ctrlIsDown{false},
      m_document{nullptr},
      m_page{nullptr}
{
    m_view->setScene(m_scene);
    m_view->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    m_view->setToolTip(tr("Drag this equation and drop it somewhere"));

    auto* l = new QVBoxLayout(this);
    l->addWidget(m_view, Qt::AlignCenter);
    setLayout(l);

    // find a better way
    setStyleSheet("QWidget:focus {"
                  "border: 2px solid rgb(59,89,152);"
                  "}");
}

EquationViewer::~EquationViewer()
{

}

void EquationViewer::setFilePath(const QString& filepath)
{
    m_filepath = filepath;
    m_isDirty = true;
}

void EquationViewer::setEquationBackground(const QColor &equation_background)
{
    m_opts.backgroundColor = equation_background;
}

void EquationViewer::setBackground(const QColor &color)
{
    if(color == Qt::transparent) {
        m_view->setBackgroundRole(QPalette::Window);
    }
    m_view->setBackgroundBrush(QBrush(color));
}

void EquationViewer::setRenderOptions(const equalx::RenderOptions &opts)
{
    m_opts = opts;
}

void EquationViewer::refresh()
{
    if(m_isDirty && !m_filepath.isNull()) {
        m_document.reset(Poppler::Document::load(m_filepath));
        if (!m_document || m_document->isLocked()) {
            QString err = "Poppler can't access PDF document at: " + m_filepath;

            throw equalx::Exception("EquationViewer::refresh", err);
        }

        m_document->setRenderHint(Poppler::Document::Antialiasing);
        m_document->setRenderHint(Poppler::Document::TextAntialiasing);
        m_document->setRenderBackend(Poppler::Document::ArthurBackend);

        m_document->setPaperColor(Qt::transparent);

        // save the page object
        m_page.reset(m_document->page(0));

        QImage thumbnail = equalx::renderPage(m_page, 24.0f, 24.0f);

        m_refAABB = equalx::get_bbox(thumbnail);

        m_isDirty = false;
    }

    if(m_curZoom < 0.1f) m_curZoom = 0.1;
    if(m_curZoom > 40.0f) m_curZoom = 40.0f;

    if(!m_page) {
        return;
    }

    // now render the full page according  to the RenderOptions
    QImage result = equalx::renderPage(m_page, m_curZoom * m_opts.xres, m_curZoom * m_opts.yres);

    // the AABB should be somewhere scaled from smaller image
    const double scalingFactorX = m_curZoom * m_opts.xres/24.0f;
    const double scalingFactorY = m_curZoom * m_opts.yres/24.0f;

    // AABB scaled from thumbnail AABB to the current resolution
    const QRect aabb_scaled(m_refAABB.x() * scalingFactorX, m_refAABB.y() * scalingFactorY,
                            m_refAABB.width() * scalingFactorX, m_refAABB.height() * scalingFactorY);

    const QRect& aabb_constrained = equalx::get_bbox(result, aabb_scaled); // computed in-image based on scaled AABB
    const QRect aabb_corrected(aabb_constrained.topLeft(), aabb_scaled.size()); // it seems this rect is closer to the directly computed AABB
    //    const QRect& aabb_computed = equalx::get_bbox(result);

    //    qDebug() << "\n\n";
    //    qDebug() << "Scaled BBox: " << aabb_scaled;
    //    qDebug() << "Constrained BBox: " << aabb_constrained;
    //    qDebug() << "Corrected BBox: " << aabb_corrected;
    //    qDebug() << "Computed BBox: " << aabb_computed;

    const QRect full_cropped_region = aabb_corrected + m_curZoom * m_opts.padding;

    //! \note this works even this code is commented, but maybe handle this in future?
    // check if the bounding box is inside the page..
    //    QRect rsize(QPoint(0,0), result.size());
    //
    //    if(!rsize.contains(full_cropping_rect)){
    //        throw equalx::Exception("EquationViewer::refresh", "Couldn't crop the equation");
    //    }

    QImage cropped(result.copy(full_cropped_region));

    result = equalx::apply_background(cropped, m_opts.backgroundColor);

    if(!m_item) {
        m_item = m_scene->addPixmap(QPixmap::fromImage(result));
        m_item->setShapeMode(QGraphicsPixmapItem::BoundingRectShape);
    }
    else {
        m_item->setPixmap(QPixmap::fromImage(result));
    }

    m_view->setSceneRect(m_item->sceneBoundingRect());
    m_view->update();

    emit refreshed();
}

void EquationViewer::setZoom(double factor)
{
    if(factor >= 0.01){
        m_curZoom = factor;
    }

    refresh();

    emit zoomChanged(m_curZoom);
}

void EquationViewer::zoomIn()
{
    if(m_curZoom > 0.0f && m_curZoom <= 4.0f){
        m_curZoom += 0.1;
    }
    else if(m_curZoom > 4.0f && m_curZoom <= 8.0f) {
        m_curZoom += 1.0;
    }
    else if(m_curZoom > 8.0f && m_curZoom <= 16.0f) {
        m_curZoom += 2.0;
    }
    else if(m_curZoom > 16.0f) {
        m_curZoom += 4.0;
    }
    else {
        m_curZoom += 8.0;
    }

    refresh();

    emit zoomChanged(m_curZoom);
}

void EquationViewer::zoomOut()
{
    if(m_curZoom > 0.1f && m_curZoom <= 4.0f){
        m_curZoom -= 0.1;
    }
    else if(m_curZoom > 4.0f && m_curZoom <= 8.0f) {
        m_curZoom -= 1.0;
    }
    else if(m_curZoom > 8.0f && m_curZoom <= 16.0f) {
        m_curZoom -= 2.0;
    }
    else if(m_curZoom > 16.0f) {
        m_curZoom -= 4.0;
    }
    else {
        m_curZoom -= 8.0;
    }

    refresh();

    emit zoomChanged(m_curZoom);
}

//! \todo uncommnet when to be implementeed completely
//void EquationViewer::keyPressEvent(QKeyEvent *ev)
//{
//    m_ctrlIsDown = (ev->key() == Qt::Key_Control);

//    QWidget::keyPressEvent(ev);
//}

//void EquationViewer::keyReleaseEvent(QKeyEvent *ev)
//{
//    m_ctrlIsDown = !(ev->key() == Qt::Key_Control);

//    QWidget::keyReleaseEvent(ev);
//}

//void EquationViewer::wheelEvent(QWheelEvent *ev)
//{
//    if(m_ctrlIsDown) {
//        if(ev->angleDelta().y() > 0) {
//            zoomIn();
//        }
//        else {
//            zoomOut();
//        }

//        ev->accept();
//    }
//    else {
//        ev->ignore();
//    }

//}




//void EquationViewer::mousePressEvent( QMouseEvent * event)
//{
//    if (event->button() == Qt::LeftButton ){
//        mDragPosition = event->pos();
//    }
//}

//void EquationViewer::mouseMoveEvent( QMouseEvent * event)
//{
//    if (!(event->buttons() & Qt::LeftButton))
//        return;

//    if (QLineF(event->pos(), mDragPosition).length() < QApplication::startDragDistance()) {
//        return;
//    }

//    if(!pixmap()) return;

//    QList<QUrl> urls;
//    QUrl fileUrl = QUrl::fromLocalFile(mDragFileSource);

//    urls.append( fileUrl );


//    auto* mimeData = new QMimeData;
//    mimeData->setUrls(urls);

//    auto* drag = new QDrag(this);
//    drag->setMimeData(mimeData);
//    drag->setPixmap( *pixmap() );
//    drag->setHotSpot( event->pos() - QPoint(static_cast<int>((width()-pixmap()->width())/2.0), static_cast<int>((height()-pixmap()->height())/2.0) ) );
//    drag->exec(Qt::CopyAction);
//}

