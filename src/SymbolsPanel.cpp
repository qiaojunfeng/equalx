/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGridLayout>

#include "SymbolsPanel.h"
#include "SymbolsGroupWidget.h"

//_________________________________________________________________________
SymbolsPanel::SymbolsPanel(QWidget *parent, int cols)
    : QWidget(parent),
      mMaxCols(cols),
      mCurCol(0),
      mCurRow(0)
{
    // allocate all widgets on the panel

    mLayout = new QGridLayout(this);
    mLayout->setContentsMargins(0,0,0,0);
    mLayout->setSpacing(3);

    setLayout(mLayout);
}

void SymbolsPanel::appendSymbolsGroup(LatexSymbolsGroup* group)
{
    auto* grWidget = new SymbolsGroupWidget(3, this);
    grWidget->addSymbols(group);

    if(mCurCol>=mMaxCols) {mCurCol=0; mCurRow++;}

    mLayout->addWidget(grWidget, mCurRow, mCurCol);

    mCurCol++;

    connect(grWidget, SIGNAL(triggered(LatexSymbol*)), this, SIGNAL(triggered(LatexSymbol*)) );
}
