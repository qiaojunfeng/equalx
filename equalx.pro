# -------------------------------------------------
# Project created by QtCreator 2009-01-30T14:36:03
# -------------------------------------------------

message(Using Qt version: $$[QT_VERSION])


CONFIG += qt gui svg c++17

DEFINES += "GIT_TAG_VERSION=\\\"$$system(git describe --abbrev=0 --tags)\\\""

# One may define a directory where to save User's storage (preambles, environments, history, library, etc)
# using definition: USER_STORAGE
#
# Uncomment line below if you want to change the user's default storage directory (~/.local/share/equalx)
# DEFINES += "USER_STORAGE=\\\"/etc/var/\\\""


unix:!macx{
DEFINES += LINUX_PLATFORM
CONFIG+= link_pkgconfig
PKGCONFIG += exempi-2.0 poppler-qt5
#INCLUDEPATH+=/usr/include/exempi-2.0
#LIBS += -L/usr/lib
OTHER_FILES+=equalx.1

SOURCES =  src/File.cpp
}

win32{
DEFINES += WIN_PLATFORM
RC_FILE = resources/resourceIconWindows.rc
OTHER_FILES+=resources/resourceIconWindows.rc

XMPSDK_PATH=C:/XMPSDK
WINSPARKLE_PATH=C:/WinSparkle-0.4

message(XMPS SDK at: $$XMPSDK_PATH)
message(WinSparkle at: $$WINSPARKLE_PATH)

INCLUDEPATH+=$$XMPSDK_PATH/public/include
INCLUDEPATH+=$$WINSPARKLE_PATH/include

LIBS+=-L"C:/Windows/system32"
LIBS+=-L"$$XMPSDK_PATH/public/libraries/windows/Release" -lXMPCore -lXMPFiles
LIBS+=-L"$$WINSPARKLE_PATH/Release" -lWinSparkle

SOURCES = src/FileWin.cpp
}

QT += widgets sql

TARGET = equalx
TEMPLATE = app

UI_HEADERS_DIR = include
UI_SOURCES_DIR = src
MOC_DIR = tmp
RCC_DIR = src

INCLUDEPATH += include \
    include/Library \
    include/BookmarksPanel \
    include/HistoryPanel \

DEPENDPATH = include/ \
    include/Library \
    include/BookmarksPanel \
    include/HistoryPanel \
    src \
    ui

INSTALLS += target
DISTFILES += resources/templates/* \
    resources/icons/menu/* \
    resources/icons/tabBar/* \
    resources/icons/mathbar/*
SOURCES += src/main.cpp \
    src/MainWindow.cpp \
    src/LatexHighlighter.cpp \
    src/LatexEditor.cpp \
    src/WidgetColorPicker.cpp \
    src/DialogReplace.cpp \
    src/DialogPreferences.cpp \
    src/DialogAbout.cpp \
    src/WidgetFind.cpp \
    src/EquationTemplateWidget.cpp \
    src/Util.cpp \
    src/RenderEngine.cpp \
    src/EquationView.cpp \
    src/EquationArea.cpp \
    src/SymbolsGroupWidget.cpp \
    src/SymbolsGroupMenu.cpp \
    src/SymbolsPanel.cpp \
    src/SearchLineEdit.cpp \
    src/Symbol.cpp \
    src/Library/LibraryData.cpp \
    src/Library/Library.cpp \
    src/BookmarksPanel/DialogPreferencesFolder.cpp \
    src/BookmarksPanel/DialogPreferencesBookmark.cpp \
    src/BookmarksPanel/BookmarksWidget.cpp \
    src/BookmarksPanel/BookmarksViewItemDelegate.cpp \
    src/BookmarksPanel/BookmarksView.cpp \
    src/BookmarksPanel/BookmarksItemModel.cpp \
    src/BookmarksPanel/BookmarkItem.cpp \
    src/HistoryPanel/HistoryWidget.cpp \
    src/HistoryPanel/HistoryViewItemDelegate.cpp \
    src/HistoryPanel/HistoryView.cpp \
    src/HistoryPanel/HistoryListModel.cpp \
    src/SegmentedControl/SegmentControl.cpp \
    src/DoubleColorSwatch.cpp \
    src/DialogHistoryClear.cpp \
    src/Settings.cpp \
    src/settings_templates.cpp \
    src/settings_preview.cpp \
    src/settings_general.cpp \
    src/settings_editor.cpp \
    src/settings_advanced.cpp \
    src/abstract_settings_group.cpp \
    src/preamblefile.cpp \
    src/bodytemplatefile.cpp \
    src/equalx_exception.cpp \
    src/FileMetadata.cpp \
    src/RenderEngineData.cpp \
    src/preambletool.cpp \
    src/environmenttool.cpp \
    src/RenderPropertiesTool.cpp
HEADERS += include/MainWindow.h \
    include/defines.h \
    include/LatexHighlighter.h \
    include/LatexEditor.h \
    include/WidgetColorPicker.h \
    include/DialogReplace.h \
    include/DialogPreferences.h \
    include/DialogAbout.h \
    include/WidgetFind.h \
    include/EquationTemplateWidget.h \
    include/Util.h \
    include/RenderEngine.h \
    include/EquationView.h \
    include/File.h \
    include/EquationArea.h \
    include/SymbolsGroupWidget.h \
    include/SymbolsGroupMenu.h \
    include/SymbolsPanel.h \
    include/Symbol.h \
    include/SearchLineEdit.h \
    include/Library/LibraryData.h \
    include/Library/Library.h \
    include/BookmarksPanel/DialogPreferencesFolder.h \
    include/BookmarksPanel/DialogPreferencesBookmark.h \
    include/BookmarksPanel/BookmarksWidget.h \
    include/BookmarksPanel/BookmarksViewItemDelegate.h \
    include/BookmarksPanel/BookmarksView.h \
    include/BookmarksPanel/BookmarksItemModel.h \
    include/BookmarksPanel/BookmarkItem.h \
    include/HistoryPanel/HistoryWidget.h \
    include/HistoryPanel/HistoryViewItemDelegate.h \
    include/HistoryPanel/HistoryView.h \
    include/HistoryPanel/HistoryListModel.h \
    include/defines_win.h \
    include/defines_linux.h \
    include/defines_mac.h \
    include/SegmentedControl/SegmentControl.h \
    include/DoubleColorSwatch.h \
    include/DialogHistoryClear.h \
    include/Settings.h \
    include/settings_templates.h \
    include/settings_preview.h \
    include/settings_general.h \
    include/settings_editor.h \
    include/settings_advanced.h \
    include/abstract_settings_group.h \
    include/defines_latex.h \
    include/preamblefile.h \
    include/bodytemplatefile.h \
    include/equalx_exception.h \
    include/FileMetadata.h \
    include/RenderEngineData.h \
    include/preambletool.h \
    include/settings_filedata.h \
    include/environmenttool.h \
    include/RenderPropertiesTool.h
FORMS += ui/mainwindow.ui \
    ui/dialogPreferences.ui \
    ui/dialogAbout.ui \
    ui/dialogReplace.ui \
    ui/widgetFind.ui \
    ui/BookmarksPanel/DialogPreferencesFolder.ui \
    ui/BookmarksPanel/DialogPreferencesBookmark.ui \
    ui/BookmarksPanel/BookmarksWidget.ui \
    ui/HistoryPanel/HistoryWidget.ui \
    ui/DialogHistoryClear.ui \
    ui/preambletool.ui \
    ui/environmenttool.ui \
    ui/RenderPropertiesTool.ui
RESOURCES += resourceFiles.qrc
OTHER_FILES += LICENSE \
    COPYING \
    README \
    THANKS \
    changelog \
    resources/stylesheet.qss \
    resources/createLibraryTables.sql \
    resources/equalx.desktop \
    equalx.appdata.xml
