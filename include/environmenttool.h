/**
 ** This file is part of the equalx project.
 ** Copyright 2020 Mihai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef ENVIRONMENTTOOL_H
#define ENVIRONMENTTOOL_H

#include <vector>

#include <QWidget>

#include "settings_filedata.h"

namespace Ui {
class EnvironmentTool;
}

class EnvironmentTool : public QWidget
{
    Q_OBJECT

public:
    explicit EnvironmentTool(QWidget *parent = nullptr);
    ~EnvironmentTool();

    const QString& getEnvBegin() const { return mAppliedTemplateBegin; }
    const QString& getEnvEnd() const  { return mAppliedTemplateEnd; }

    void setEquationTemplates(const std::vector<equalx::SettingsFileData>& templates);
    void setEnvBegin(const QString& text);
    void setEnvEnd(const QString& text);

    // settings for the Editor (Preamble PlainText)
    void setEditorHighlighting(bool value=true);
    void setEditorCompletion(bool value=true);
    void setEditorCompletionCaseSensitive(bool value=true);
    void setEditorFont(const QFont& font);

signals:
    void reset();
    void apply();

private slots:
    void onReset();
    void onApply();
    void onTemplateSelected(int index);

private:
    Ui::EnvironmentTool *ui;
    // variables used for Reset functionality
    QString mOrigTemplateBegin; //! original Template's begin
    QString mOrigTemplateEnd; //! original Template's end

    // saved (applied) variables
    QString mAppliedTemplateBegin;
    QString mAppliedTemplateEnd;
};

#endif // ENVIRONMENTTOOL_H
