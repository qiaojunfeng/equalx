/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBRARYDATA_H
#define LIBRARYDATA_H

#include <QDebug>
#include <QDataStream>
#include <QMetaType>
#include <QVariant>

/** These data is intended to be used for fast viewing (and low memory consumption in models) **/

class LibraryModelData
{
public:
    enum Type
    {
        TypeUndefined,
        TypeFolder,
        TypeHistory,
        TypeBookmark
    };


    LibraryModelData();
    LibraryModelData(const LibraryModelData& other) =default;
    virtual ~LibraryModelData()=default;

    bool isFolder() const;
    bool isBookmark() const;
    bool isHistory() const;
    bool isValid() const; // is valid if the object is in database

    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }

    int id;
    int idparent;
    QString name; // this field can be (depending on context) the title of a library folder or the absolute path to an equation file
    QString filePath;
    QString description;
    LibraryModelData::Type type;
    qint64 created;
};
Q_DECLARE_METATYPE(LibraryModelData)

typedef QList<LibraryModelData> LibraryRowsList;

inline QDataStream& operator<<(QDataStream& out, const LibraryModelData& object)
{
    out << object.id;
    out << object.idparent;
    out << object.name;
    out << object.filePath;
    out << object.description;
    out << static_cast<quint32>(object.type);
    out << object.created;

    return out;
}

inline QDataStream& operator>>(QDataStream& in, LibraryModelData& object)
{
    in >> object.id;
    in >> object.idparent;
    in >> object.name;
    in >> object.filePath;
    in >> object.description;
    quint32 typeIntVal;

    in >> typeIntVal;
    object.type= static_cast<LibraryModelData::Type>(typeIntVal);

    in >> object.created;
    return in;
}

QDebug operator<<(QDebug dbg, const LibraryModelData &data);

//_____________________________________________________________________________________________________
/**
 **  The data types below are intended for complete editing/viewing of a specific library item
 **
 **/
class HistoryRow
{
public:
    HistoryRow();
    HistoryRow(const HistoryRow& other) = default;
    virtual ~HistoryRow()=default;

    LibraryModelData basicData() const;

    int id;
    QString title;
    QString filePath;
    qint64 created;
};
Q_DECLARE_METATYPE(HistoryRow)


class Bookmark
{
public:
    Bookmark();
    Bookmark(const Bookmark& other) =default;
    Bookmark(const LibraryModelData& data);
    virtual ~Bookmark()=default;

    LibraryModelData basicData() const;

    bool isValid() const;
    QString fileExt() const;
    void clear(); // set members to default values (invalidates the bookmark)

    int id;
    int idparent;
    QString title;
    QString description;
    QString filePath;
    QString dirPath;
    qint64 created;
    qint64 modified;
};
Q_DECLARE_METATYPE(Bookmark)

class BookmarkFolder
{
public:
    BookmarkFolder();
    BookmarkFolder(const BookmarkFolder& other)=default;
    BookmarkFolder(const LibraryModelData& data);
    virtual ~BookmarkFolder() = default;

    LibraryModelData basicData() const;

    bool isValid() const;

    int id;
    int idparent;
    QString name;
    QString description;
    QString dirPath;
    qint64 created;
    qint64 modified;
};
Q_DECLARE_METATYPE(BookmarkFolder)


#endif // LIBRARYDATA_H
