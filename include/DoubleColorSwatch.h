#ifndef COLORCHOOSER_H
#define COLORCHOOSER_H

#include <QFrame>
#include <QColor>
#include <QMenu>

class DoubleColorSwatch : public QFrame
{
    Q_OBJECT
    
public:
    enum CHOICE{
        CHOICE_FG,
        CHOICE_BG,
        CHOICE_NONE
    };

    DoubleColorSwatch(QWidget *parent = 0, Qt::WindowFlags f = nullptr);
    ~DoubleColorSwatch()=default;

    QColor fgColor() const { return mFgColor; }
    QColor bgColor() const {return mBgColor; }

    void setFgColor(const QString& name);
    void setFgColor(const QColor& color);
    void setBgColor(const QString& name);
    void setBgColor(const QColor& color);

    QSize	sizeHint () const;

signals: // SIGNALS
    void fgColorChanged(QColor);
    void bgColorChanged(QColor);

private slots:
    void onColorChosen(QAction* act);
    void showColorsDialog();

protected:
    bool event(QEvent *event);
    void paintEvent(QPaintEvent *event);
    void resizeEvent ( QResizeEvent * event );

    void mousePressEvent ( QMouseEvent * event );

    void showMenu(DoubleColorSwatch::CHOICE chosen);
private:
    void setupColorsMenu();
    void addMenuAction(const QColor& fillColor, const QString& name);
    QIcon createIcon(const QColor& fillcolor) const;

    QColor mFgColor;
    QColor mBgColor;
    QColor mActiveColor; // color of active Area

    QRect mFgArea;
    QRect mBgArea;
    QLine mSeparator;
    QMenu mColorsMenu;
    DoubleColorSwatch::CHOICE mSwatch; // identifies which swatch is active (FG or BG)

    int mPadding; // widgets padding
    int mSeparatorWidth; // spacer width between color swatches
};

#endif // COLORCHOOSER_H
