/*
 * Copyright 2013-2019 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FILEINFO_H
#define FILEINFO_H

#include <QColor>
#include <QString>

#include "defines_latex.h"

namespace equalx {

/*
 * An EqualX File is any file which contains EqualX metadata
 *
 * This class only describes the metadata specific for an EqualX file. For reading/writing EqualX Files look in File.h
 *
 */
class FileMetadata
{
public:
    FileMetadata();
    FileMetadata(const QString& preamble, const QString& equation, equalx::ENVIRONMENT_MODE mode=equalx::ENVIRONMENT_MODE::Display);

    // getters
    const QString& equation() const { return mEquation; }
    const QString& preamble() const { return mPreamble; }
    const QColor& fgColor() const { return mFGColor; }
    const QColor& bgColor() const { return mBGColor; }
    equalx::FONT_SIZE fontSize() const { return mFontSize; }
    equalx::ENVIRONMENT_MODE environmentMode() const { return mEnvironment; }
    const QString& envBegin() const { return mEnvBegin; }
    const QString& envEnd() const { return mEnvEnd; }

    // setters
    void setEquation(const QString& equation) { mEquation = equation; }
    void setPreamble(const QString& preamble) { mPreamble = preamble; }
    void setForegroundColor(const QColor& color){ mFGColor = color; }
    void setBackgroundColor(const QColor& color){ mBGColor = color; }
    void setFontSize(equalx::FONT_SIZE size) { mFontSize = size; }
    void setEnvironmentMode(equalx::ENVIRONMENT_MODE env) { mEnvironment = env; }
    void setEnvBegin(const QString& text) { mEnvBegin = text; }
    void setEnvEnd(const QString& text){ mEnvEnd = text; }

private:
    QString mPreamble;
    QString mEquation;
    QColor mFGColor; // color is in format for latex color
    QColor mBGColor; // color is in format for latex color
    equalx::FONT_SIZE mFontSize; // font size
    equalx::ENVIRONMENT_MODE mEnvironment; // latex environment for equation
    QString mEnvBegin;
    QString mEnvEnd;
};

}
#endif // FILEINFO_H
