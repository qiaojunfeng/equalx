/*
 * Copyright 2010-2019 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEFINES_H
#define DEFINES_H

#define APP_FULL_NAME "EqualX - The LaTeX Equation Editor"
#define APP_NAME "EqualX"
#define APP_SITE "http://equalx.sourceforge.net/"
#define AUTHOR_NAME "Mihai Niculescu"
#define AUTHOR_EMAIL "q.quark@gmail.com"

/*
*   DEFAULT FACTORY SETTINGS for QSettings
*/



/*
 * defines for parsing the .tex file
 */
#define TEMP_FILE_NAME "equation"
#define TEMP_LATEX_FILE "equation.tex"
#define TEMP_LATEX_CROP_FILE "equation-cropped.tex"
#define TEMP_PDF_FILE "equation.pdf"
#define TEMP_PDFCROP_FILE "equation-cropped.pdf"
#define TEMP_METADATA_FILE "metadata.xmp"

// METADATA
#define METADATA_NS "http://ns.equalx/meta"
// XMP schema
#define METADATA_PREFIX "equalx"
#define METADATA_PREAMBLE "preamble"
#define METADATA_EQUATION "equation"
#define METADATA_ENV "environment"
#define METADATA_FG "color"
#define METADATA_BG "background"
#define METADATA_DOC_FONT_SIZE "fontsize"

// Library Storage
#define DB_NAME "library.db"
#define BOOKMARKS_DIR "bookmarks"
#define HISTORY_DIR "history"
#define BOOKMARKSTOOLBAR_DIR "bookmarks-toolbar"
#define TEMPLATES_DIR "templates"
#define PREAMBLES_DIR "preambles"
#define ENVIRONMENTS_DIR "environments"

// include platform specific defines

#ifdef LINUX_PLATFORM
#include "defines_linux.h"
#endif

#ifdef WIN_PLATFORM
#include "defines_win.h"
#endif

#ifdef MAC_PLATFORM
#include "defines_mac.h"
#endif

#endif // DEFINES_H
