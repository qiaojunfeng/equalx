#ifndef ABSTRACT_SETTINGS_GROUP_H
#define ABSTRACT_SETTINGS_GROUP_H

#include <QString>

class QSettings;

namespace equalx {

class AbstractSettingsGroup
{
public:
    virtual ~AbstractSettingsGroup();

    virtual QString name() const =0; //! \brief returns Group's name

    //! \brief permanent storage directory for user's documents (preambles, templates, history, library, etc)
    static QString default_storage();

    //! \brief directory where the application does all work (generates temporary files, creates files, etc..)
    static QString default_workspace();
protected:
    virtual void load(QSettings& file) =0; //! \brief loads/reads its data from file
    virtual void save(QSettings& file) =0; //! \brief saves/writes its data to file
    virtual void reset()=0; //! \brief reset its data to Factory Settings
};

} // end ns equalx

#endif // ABSTRACT_SETTINGS_GROUP_H
