#ifndef TOOLWIDGETRENDERPROPERTIES_H
#define TOOLWIDGETRENDERPROPERTIES_H

#include <vector>

#include <QWidget>

#include "RenderEngineData.h"

namespace Ui {
class ToolWidgetRenderProperties;
}

class RenderPropertiesTool : public QWidget
{
    Q_OBJECT

public:
    explicit RenderPropertiesTool(QWidget *parent = nullptr);
    ~RenderPropertiesTool();

    // Setters
    void setConfigurations(const std::vector<equalx::RenderEngineConfiguration>& list);
    void setDefaultConfiguration(std::vector<equalx::RenderEngineConfiguration>::size_type pos);
    void setExportTypes(const QStringList& list);
    void setDefaultType(const QString& type);

    // Getters
    int getDefaultConfigurationIndex() const;
    QString getExportType() const;
    int getResolutionX() const;
    int getResolutionY() const;
    void getResolution(int &rx, int &ry);
    QMargins getPaddings() const;

signals:
    void apply();
    void reset();

protected slots:
    void onConfigurationSelected(int index);
    void onReset();


private:
    equalx::RenderEngineConfiguration& currentConfiguration();
    void loadMargins(const QMargins& margins);
    void selectType(const QString& type);
    void setResolution(int rx, int ry);

    Ui::ToolWidgetRenderProperties *ui;
    std::vector<equalx::RenderEngineConfiguration> mOrigConfList; // a copy to store original data (used for reset functionality)
    std::vector<equalx::RenderEngineConfiguration> mCurrentConfList; // current represented data
    std::vector<equalx::RenderEngineConfiguration>::size_type mOrigDefaultConf;
    QString mOrigDefaultType;
};

#endif // TOOLWIDGETRENDERPROPERTIES_H
