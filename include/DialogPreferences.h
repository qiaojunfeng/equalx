/*
 * Copyright 2010-2020 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIALOGPREFERENCES_H
#define DIALOGPREFERENCES_H

#include <memory>
#include <vector>

#include <QDialog>
#include <QTemporaryFile>

#include "Settings.h"

class QAbstractButton;
class QButtonGroup;
class QListWidgetItem;

namespace Ui {
class DialogPreferencesUI;
}

class DialogPreferences : public QDialog
{
    Q_OBJECT

    enum PREFERENCES_ITEM {
        ITEM_GENERAL=0,
        ITEM_EDITOR,
        ITEM_PREVIEW,
        ITEM_PREAMBLE,
        ITEM_ADVANCED
    };

public:
    DialogPreferences(const equalx::Settings& settings, QWidget *parent=nullptr);
    virtual ~DialogPreferences();

    equalx::Settings getSettings() const; // read all UI, set the settings and return them

private slots:
    void onDialogButton(QAbstractButton* b); // handle reset(), resetToFactory() or OK

    void onPageActivated(QListWidgetItem *item);

    // preambles
    void onPreambleAdd();
    void onPreambleRemove();
    void onPreambleSelected(); // save before selected and load current selected
    void onPreambleUseDefault();
    void onPreambleContentChanged();
    void onPreambleItemChanged(QListWidgetItem* item);

    // body templates
    void onBodyTemplateAdd();
    void onBodyTemplateRemove();
    void onBodyTemplateSelected(); // save before selected and load current selected
    void onBodyTemplateBeginTextChanged();
    void onBodyTemplateEndTextChanged();
    void onBodyTemplateItemChanged(QListWidgetItem* item);

    // Configurations
    void onConfigurationAdd();
    void onConfigurationClone();
    void onConfigurationRemove();
    void onConfigurationSelected();
    void onConfigurationMakeDefault();

    // Typesettings
    void onConverterPathBrowse();
    void onConverterPathChanged(const QString &newPath);
    void onConverterArgAdd();
    void onConverterArgRemove();
    void onConverterArgsChanged(QListWidgetItem *item);

    // scripts
    void onScriptStageChange(int i);

    // render options
    void onResolutionXChanged(int i);
    void onResolutionYChanged(int i);

    void onPaddingLeftChanged(int i);
    void onPaddingTopChanged(int i);
    void onPaddingRightChanged(int i);
    void onPaddingBottomChanged(int i);

private:
    void loadFromSettings(const equalx::Settings& settings); // read settings and sets up all UI elements accordingly
    void saveToSettings(equalx::Settings& settings);

    void setPagesWidget();
    void setSignals(); // setup all UI related signals specific to Dialog functionality
    void setDefaultConfiguration(unsigned int row);

    Ui::DialogPreferencesUI* ui;

    QButtonGroup* renderModeButtonGroup;
    equalx::Settings mSettings; // current Settings.

    //! \brief keep a reference to original Settings.
    //! \warning this is needed by reset().
    const equalx::Settings& mSettingsOrig;

    //! \brief temporary preambles on disk ( to be copied to preambles dir when the user accepts the changes)
    std::vector<std::unique_ptr<QTemporaryFile>> m_temp_preambles;

    //! \brief temporary bodytemplates on disk (to be copied to bodytemplates dir when the user accepts the changes)
    std::vector<std::unique_ptr<QTemporaryFile>> m_temp_bodytemplates;

    // a list of the files to be removed from disk
    std::vector<QString> m_files_ToBeRemoved;
};

#endif // DIALOGPREFERENCES_H
