#ifndef DEFINES_WIN_H
#define DEFINES_WIN_H

// Editor
#define DEFAULT_FONT "Arial,10,-1,5,50,0,0,0,0,0"

// Latex
#define DEFAULT_PDFLATEX "pdflatex.exe"
#define DEFAULT_PDFLATEX_ARGS "-interaction=nonstopmode"
#define DEFAULT_PDFCAIRO "pdftocairo.exe"
#define DEFAULT_PDFCAIRO_ARGS ""
#define DEFAULT_GS "gs.exe"
#define DEFAULT_GS_ARGS ""

#endif // DEFINES_WIN_H
