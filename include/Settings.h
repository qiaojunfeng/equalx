/*
 * Copyright 2010-2019 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EQUALXSETTINGS_H
#define EQUALXSETTINGS_H

#include <memory>
#include <QString>

namespace equalx
{

class SettingsGeneral;
class SettingsEditor;
class SettingsPreview;
class SettingsTemplates;
class SettingsAdvanced;

using SettingsGeneralPtr = std::unique_ptr<SettingsGeneral>;
using SettingsEditorPtr = std::unique_ptr<SettingsEditor>;
using SettingsPreviewPtr = std::unique_ptr<SettingsPreview>;
using SettingsTemplatesPtr = std::unique_ptr<SettingsTemplates>;
using SettingsAdvancedPtr = std::unique_ptr<SettingsAdvanced>;

/*!
 * \class Settings - The Settings Manager
 * \brief contains the in-memory representation of all settings required
 * by EqualX
 * \details besides in-memory representation of all settings it also does all the IO save/load functionality
 */
class Settings
{
public:
    Settings();
    Settings(Settings const& other);
    Settings(Settings&& other) noexcept;

    // implement assignment & move
    Settings& operator=(const Settings& other);
    Settings& operator=(Settings&& other) noexcept;

    /*
     * Getters
     */
    //! \brief General Settings
    SettingsGeneralPtr const& general() const { return mGeneral; }

    //! \brief Editors Settings
    SettingsEditorPtr const& editor() const { return mEditor; }

    //! \brief preview settings
    SettingsPreviewPtr const& preview() const { return mPreview; }

    //! \brief templates settings
    SettingsTemplatesPtr const& templates() const { return mTemplates; }

    //! \brief advanced settings
    SettingsAdvancedPtr const& advanced() const { return mAdvanced; }

    /*
     * Setters
     */
     //! \brief setter for general settings
    //! \brief General Settings
    SettingsGeneralPtr& general() { return mGeneral; }

    //! \brief Editors Settings
    SettingsEditorPtr& editor() { return mEditor; }

    //! \brief preview settings
    SettingsPreviewPtr& preview() { return mPreview; }

    //! \brief templates settings
    SettingsTemplatesPtr& templates() { return mTemplates; }

    //! \brief advanced settings
    SettingsAdvancedPtr& advanced() { return mAdvanced; }


    /*
     * Others
     */
    void load();

    /*!
     * \brief saves all settings
     *
     * \throws equalx::Exception if an error occcurs (especially, if the settings
     *  file is not writable or while writing the settings)
     */
    void save();

    void saveGeneral();
    void saveEditor();
    void savePreview();
    void saveTemplates();
    void saveAdvanced();

    void reset(); // reset to factory settings

    void setup_storage();
private:
    // required for QSettings
    QString mOrganisation;
    QString mApp;

    // need these members to be pointers in order to use move fuctionality
    /* Main Window */
    SettingsGeneralPtr mGeneral;

    /* Editor */
    SettingsEditorPtr mEditor;

    /* Preview */
    SettingsPreviewPtr mPreview;

    /* Preambles & Templates */
    SettingsTemplatesPtr mTemplates;

    /* Converters, Renders, more advanced options.. */
    SettingsAdvancedPtr mAdvanced;
};

} // end ns equalx
#endif // EQUALXSETTINGS_H
