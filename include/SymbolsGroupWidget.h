/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIDGETTOOLMENU_H
#define WIDGETTOOLMENU_H

#include <QFrame>

#include "Symbol.h"


class QAction;
class QHBoxLayout;
class QToolButton;
class QPushButton;
class QSignalMapper;

class SymbolsGroupMenu;

class SymbolsGroupWidget : public QFrame
{
    Q_OBJECT

public: // Methods
    SymbolsGroupWidget(QWidget * parent = nullptr);
    SymbolsGroupWidget(int nVisibleCols, QWidget * parent = nullptr);
    virtual ~SymbolsGroupWidget()=default;

    void addSymbols(LatexSymbolsGroup* gr);
    void setIconSize(const QSize &size);

protected: // methods
    void init(int ncols);
    void addButton(QPushButton *button);
    void addSymbol(LatexSymbol* sym);

private: // data Members

    // these are children owned by this QFrame
    QHBoxLayout *mLayout; // layout used in container widget
    LatexSymbolsGroup* mSymbolsGroup;
    SymbolsGroupMenu *mMenu;
    QSignalMapper *mSignalMapper;

    QSize mIconSize;

    int mMaxVisibleCols; // total number of visible columns shown by widget
    int mMaxSymbols;
    int mCurCol;



protected:
    // virtual void mousePressEvent(QMouseEvent *event);
private slots:
    void symbolTriggered(int id);

signals:
    void triggered(LatexSymbol*);

};

#endif // WIDGETTOOLMENU_H
