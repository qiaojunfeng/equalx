#ifndef DIALOGHISTORYCLEAR_H
#define DIALOGHISTORYCLEAR_H

#include <QDialog>

namespace Ui {
class DialogHistoryClear;
}

class DialogHistoryClear : public QDialog
{
    Q_OBJECT

public:
    explicit DialogHistoryClear(QWidget *parent = 0);
    ~DialogHistoryClear();

    enum ClearRange{
        CLEAR_HOUR=0x0,
        CLEAR_TODAY=0x1,
        CLEAR_EVERYTHING=0x2
    };

    int clearRangeType() const;

private:
    Ui::DialogHistoryClear *ui;
};

#endif // DIALOGHISTORYCLEAR_H
