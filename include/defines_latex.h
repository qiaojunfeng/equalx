/*
 * Copyright 2019 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEFINES_LATEX_H
#define DEFINES_LATEX_H

namespace equalx {

enum ENVIRONMENT_MODE {
    Display=0,
    Inline,
    Align,
    Text,
    User
};

enum FONT_SIZE {
    Tiny=0,
    Script,
    Footnote,
    Small,
    Normal,
    Large,
    VeryLarge,
    Huge,
    VeryHuge
};

} // end ns equalx

#endif // DEFINES_LATEX_H
