/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SYMBOL_H
#define SYMBOL_H

#include <QString>
#include <QStringList>
#include <QList>

#include "defines_latex.h"


class LatexSymbolsGroup;

class LatexSymbol
{
public:
    LatexSymbol();
    virtual ~LatexSymbol()
    {
        parent = nullptr;
        packages.clear();
    }

    int id; // internal counter
    LatexSymbolsGroup* parent; // pointer to parent group
    QString name;
    QString icon; // icon file name
    QString latex; // latex symbol
    QStringList packages; // required latex packages
};

class LatexSymbolsGroup
{
public:
    LatexSymbolsGroup();
    virtual ~LatexSymbolsGroup()
    {
        parent = nullptr;

        while(!symbols.isEmpty()) delete symbols.takeFirst();

        symbols.clear();
    }

    unsigned int id;
    LatexSymbolsGroup* parent;
    QString name;
    QList<LatexSymbol*> symbols;
};

class LatexEquation : public LatexSymbol
{
public:
    LatexEquation();
    virtual ~LatexEquation();

    QString preamble;
    QStringList tags;
    //QColor fg;
    //QColor bg;
    equalx::ENVIRONMENT_MODE envMode;
    equalx::FONT_SIZE fontSize;


};

#endif // SYMBOL_H
