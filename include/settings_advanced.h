/**
 ** This file is part of the equalx project.
 ** Copyright 2019 Mihai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef SETTINGS_ADVANCED_H
#define SETTINGS_ADVANCED_H

#include <optional>
#include <vector>

#include <QString>

#include "RenderEngineData.h"
#include "abstract_settings_group.h"

namespace equalx {

using RenderEngineConfigurationList = std::vector<RenderEngineConfiguration>;

class SettingsAdvanced : public AbstractSettingsGroup {
    friend class Settings;
public:
    QString name() const override { return "ADVANCED"; }

    RenderEngineConfigurationList::size_type getDefaultConfigurationIndex() const;
    std::optional<RenderEngineConfiguration> getDefaultConfiguration() const;
    const RenderEngineConfigurationList& getAllConfigurations() const;

    RenderEngineConfigurationList::size_type createNewRenderEngineConfiguration(const QString& name = "Untitled");
    RenderEngineConfigurationList::size_type createCloneRenderEngineConfiguration(RenderEngineConfigurationList::size_type i, const QString& name = "Untitled");
    const RenderEngineConfiguration &getRenderEngineConfiguration(RenderEngineConfigurationList::size_type i) const; //! \brief get RenderEngineConfiguration at position \a i in the array
    RenderEngineConfiguration &getRenderEngineConfiguration(RenderEngineConfigurationList::size_type i);
    RenderEngineConfigurationList::size_type countRenderEngineConfiguration() const;
    void removeRenderEngineConfiguration(RenderEngineConfigurationList::size_type i); //! \brief remove RenderEngineConfiguration at position \a i in the array


    // permanent storage for user's documents:
    QString storage_bookmarks() const;
    QString storage_toolbar() const;
    QString storage_history() const;

    void setDefaultRenderEngineConfiguration(const RenderEngineConfigurationList::size_type defaultRenderEngineConfiguration);

    void load(QSettings &settings)  override; //! \brief loads/reads its data from file
    void save(QSettings &settings)  override; //! \brief saves/writes its data to file
    void reset() override; //! \brief reset its data to Factory Settings

private:
    //! \brief Constructs an Advanced Settings having the default factory settings
    SettingsAdvanced();

    //! \brief Constructs an Advanced Settings having the settings read from QSettings file
    SettingsAdvanced(QSettings &file);

    RenderEngineConfigurationList m_RenderEngineConfigurations;
    RenderEngineConfigurationList::size_type m_defaultRenderEngineConfiguration;
};

}

#endif // SETTINGS_ADVANCED_H
