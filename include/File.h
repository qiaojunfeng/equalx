/*
 * Copyright 2013 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EQUALXFILE_H
#define EQUALXFILE_H

#include <QString>
#include <QStringList>

#include "defines.h"
#include "FileMetadata.h"

namespace equalx {

/*
 * A EqualX File is any file which contains  EqualX metadata
 *
 * This class is used for generating data/files required for reading/writing files with metadata specific for EqualX
 * The main purpose of this class is for I/O operations. The description of EqualX metadata is in class EqualX::FileInfo
 */
class File
{
public:
    enum OpenMode{
        OPEN_READ=0x0,
        OPEN_UPDATE=0x1,// READ-WRITE
        OPEN_SMART=0x2, // use smart file handler
        OPEN_SCAN=0x4   // scan for metadata
    };
    Q_DECLARE_FLAGS(OpenModes, OpenMode)

    File();
    File(const equalx::FileMetadata& fileinfo);
    ~File();

    void open(const QString& filename, equalx::File::OpenModes mode=equalx::File::OPEN_UPDATE);
    void close();

    bool read(); // read metadata from the current file and set FileInfo and Metadata
    bool write();// write Metadata into the current file

    const equalx::FileMetadata& data() const;
    void setData(const equalx::FileMetadata& fileinfo);

    // write methods for the current FileInfo
    // the files are saved in the current working dir and named according to defines.h
    void writeLatexFile(bool withBackgroundColor=false); // create a latex file
    void writeLatexFileCropped(float llx, float lly, float urx, float ury); // boundingbox: lowerleft x, lowerleft y, upperright x, upperright y
    void writeMetadataFile(); // a metadata file to be embedded into files

    static bool fetchInfo(const QString& filename, equalx::FileMetadata& fileInfo);
    static bool fetchInfo(const char* buffer, size_t len, equalx::FileMetadata *data);

    static QStringList supportedFileTypes() { return mSupportedFileTypes; }
protected:
    void __init();

private:
    QString mFileName;
    equalx::FileMetadata mFileInfo;

    struct FileImplementation;
    FileImplementation* mClassImpl{nullptr};
    static QStringList mSupportedFileTypes;
};

typedef QFlags<equalx::File::OpenMode> EqualXFileOpenMode;

Q_DECLARE_OPERATORS_FOR_FLAGS(File::OpenModes)

} // namespace equalx
#endif // EQUALXFILE_H
