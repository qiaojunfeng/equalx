equalx (0.80-0) xxx; urgency=low

    - dropped support for highlight selections


equalx (0.72-0) utopic; urgency=low

    - Enviroment Mode buttons from MainWindow are now rendered as SegmentedControl, as in Mac OS X
    - corrected Font size combobox in MainWindow to show with the "prefered" size (it was too big when in fullscreen or maximized)
    * Added a Home Page in main window with a small explanation on the usage - useful for beginners
    - Moved the "New equation" button near the Refresh, also the mainbar was moved and is shown only when an equation is visible
    * fixed whole Replace Dialog functionality
    - preparing a definitions for when porting to Mac
    - renamed ColorChooser to DoubleColorSwatch.h
    - Added tooltips for DoubleColorSwatch, Refresh button, Font size
    - new icons for bookmark star
    - SymbolsGroupWidget loads the symbols from local "resources/symbols" dir or if defined, from "RESOURCES_DIR/symbols"
    * fixed desktop icon for KDE. This created errors after launching the application, see Bug #1360714
    - modifed the look of About Dialog. Now showing only a credits button and the THANKS file
    - added in THANKS file more people which had important contributions, and improved its appearnce


equalx (0.62-0) oneiric; urgency=low

  - corrected transparent background bug for latex
  - corrected square brackets
  - corrected quadratic formula from templates
  - Equation font size defaults to size 20
  - empty text in equation editor shows information text
  - solved minor bugs in output of latex log
  * Implemented zoom & zoom slider
  * Preferences Dialog allows to show/hide Log
  * Latex output visible in Log Tab
  - polished GUI
  * Now Preamble can be toggle from a button in mainwindow
  * Preferences allows to show/hide only the latex log tab
  * LatexEditor shows line numbers, same for the Preamble editor.
  But, in Equation editor the counting starts from where preamble finishes.
  * Reported errors by latex output are highlighted in Equation editor and
  Preamble editor. A nice sign shows along the line number also.
  - fixed warnings
  * some modifications to About Dialog. Included Like button for the EqualX Facebook page
  * Fixed About Dialog links
  - corrected "Remeber" in Preferences Dialog
  * Added class ConfigButton required for a polished GUI in the Modified tab "Commands"
  from Preferences Dialog.
  * Added the GPL 3 License boilerplate to all remaining files
  * Added a check for the requirements (latex, dvipng,etc)
  - commands paths in Preferences Dialog are now readonly. They can be changed only the "Change" buttons
  - Change buttons for the commands select only the specific executables (filters other files)
  * Added fg color, bg color, preview font size and mode in Preferences dialog. which are saved in the settings
  * Added button "Reset Defaults" to the Preferences dialog and works
  - removed Util function from defines.h and moved the Util functions in Util.h
  - fixed a bug in Preferences dialog to set Font family and size for the equation editor
  - corrected UpdateEquation and UpdateImage to call runCommand(). Preparing to run the commands in a separete thread
  * Added more environments: display, inline(which were before) and align and text. Select them from the Mainwindow and Preferences
  * modified UI mainwindow and polished it for better smaller layout
  * removed Latex log output and tabs from mainwindow. There is no need since the error parser is enough
  * removed Parser and latex-template.
  * Modified Zoom slider to show zoom level in a tooltip
  - solved bug for find and highlight when dealing with transparent background color. If color is transparent than highlight in white.
  - fixed WidgetColorPicker.h to show real swatch transparent color.
  - fixed custom color selection in WidgetColorPicker
  - Modified foreground color combobox to not show transparent option (latex can not render transparent color)
  * Implemented a RenderEngine class to deal with the rendering and programs execution
  * Fixed RenderEngine
  * Added a nice preloader to show instead of the equation while the RenderEngine generates the files
  - cleaned up some obsolete and unused code in MainWindow
  - Modified RenderEngine to not take care of latex exitcode. This is required so that we parse and display the latex errors
  - changed preloader image with a nicer one
  - Functional preview of the generated image PNG
  - Modified RenderEngine to do check for the latex errors and stop if errors are found.
  - Modified EquationItem to clear if no image is available. Equation item now shows a grab cursor (hand) if it is possible to drag it, normal cursor otherwise
  - Modified Mainwindow and Equation item in order to not be possible to drag empty equation
  - Mainwindow shows a busy cursor while the RenderEngine is running




 -- Mihai Niculescu <q.quark@gmail.com>  Sat, 22 Oct 2011 17:50:00 +0200


equalx (0.51-0) oneiric; urgency=low

  - Solved some minor bugs
  * Solved Recent Equations List bug (no empty equations in list)
  - add equation to Recent Files List after saved
  * Solved a bug from WidgetColor Picker
  * Added Changelog to EqualX Qt project file
  * Modified initial main window layout
  * Solved export bugs
  * Created binary packages for Windows & Ubuntu



 -- Mihai Niculescu <q.quark@gmail.com>  Sat, 22 Oct 2011 17:50:00 +0200

equalx (0.50-1) oneiric; urgency=low

  * Redesigned Main window layout
  * Added Find Widget and a Replace Dialog
  * Added Search Menu items
  * Added Export Menu items
  * Added Export commands in Preferences Dialog
  * Added Manual Preview mode (available in Main Window & Preferences Dialog)
  * Added Preview button for manual Preview
  * Added Forced Preview available when pressing F5 key
  * Added Forced Preview in View menu item
  * Modified font size computed accordingly to desktop font DPI
  * Modified About Dialog
  * Removed Help->Manual menu item
  * Modified Symbols & Math Panel - make them 2 different toolbars
  * Modified background Symbols Toolbar to a stylesheet
  * Modified source code structure
  

 -- Mihai Niculescu <q.quark@gmail.com>  Sat, 22 Oct 2011 01:08:00 +0200

equalx (0.42-1) lucid; urgency=low

  * Added an icon for EqualX in Windows
  * Changed Main window GUI
  * ported to Windows
  * Drag and drop bugs resolved
  - minor bugs solved

 -- Mihai Niculescu <q.quark@gmail.com>  Sun, 24 Nov 2010 02:24:00 +0200

equalx (0.4-1) lucid; urgency=low

  * Removed Library Classes, no need for them yet.
  * Removed sqlite dependency in the project file, no need yet
  * Added Preferences Dialog UI (functionality almost complete)
  * New possible ways to customize the layout of the window
  * New About Dialog
  * Added Recent Files in Equation Menu
  * Fixed some bugs for writing and loading the equation from PNG metadata
  * Added undo, redo, copy, etc...
  - Modified Symbols & Math Panel

 -- Mihai Niculescu <q.quark@gmail.com>  Sun, 21 Nov 2010 14:36:00 +0200

equalx (0.3-1) karmic; urgency=low

  * Initial release

 -- Mihai Niculescu <q.quark@gmail.com>  Wed, 03 Mar 2010 15:59:39 +0200
